﻿import System.Xml;
import System.Xml.Serialization;

public class Position {
	@XmlAttribute("Coordinate")
	public var X : int;
	public var Y : int;
}