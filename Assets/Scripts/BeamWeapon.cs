using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent (typeof(DamageDealer))]
public class BeamWeapon : Weapon, IShootable {
    [System.Serializable]
    public class Beam
    {
        public List<LineRenderer> beamList = new List<LineRenderer>();


        public Beam instantiate(Transform transform)
        {
            List<LineRenderer> beams = new List<LineRenderer>();
            for (int i = 0; i < beamList.Count; i++ )
            {
                LineRenderer b = ((GameObject)GameObject.Instantiate(beamList[i].gameObject, transform.position, transform.rotation)).GetComponent<LineRenderer>();
                b.transform.parent = transform;
                beams.Add(b);
            }
            Beam newbeam = new Beam();
            newbeam.beamList = beams;
            return newbeam;
        }
        public void enable(){
            foreach (LineRenderer lr in beamList)
                lr.enabled = true;
        }
        public void disable(){
            foreach (LineRenderer lr in beamList)
                lr.enabled = false;
        }
        public void SetPosition(int index, Vector3 position)
        {
            beamList.ForEach(lr => lr.SetPosition(index, position));
        }
    }
	public GameObject endPoint;
	public GameObject spawnPoint;
    [SerializeField]
    public Beam lineRendererPrefab = new Beam();        //rendered in layers from first to last
	public float shotsPerSec = 1;			//shots per second
	public float shotReloadTime = 0.5f;	//time (seconds) to reload after burst
	public float maxBurst = 3f;				//max number of projectiles in burst
	private Beam[] activeBeams;
	public AudioSource startSound;
	public AudioSource loopSound;
	public AudioSource endSound;
    public int rays = 4;
	public float range = 100f;

    
	private float currentBurst = 0f;
	private float shootTimer=0f;
	private float reloadTimer=0f;
	public bool fire = false;	
	Vector2 maxDistVector = new Vector2(0,100f);

	public ParticleSystem hitEffect;
	Transform hitEffectTransform;
	
	public ResourceMeta.Type ammoType;
	public int ammoRate = 1;
	public ResourceContainer primaryContainer;
	public LayerMask lMask;
    private int activeRay;

    protected DamageDealer damageDealer;
	
	// Use this for initialization
	public override void Start () {
        base.Start();
        		
		//lineR = GetComponent<LineRenderer>();
        activeBeams = new Beam[rays];
        for (int i = 0; i < rays; i++)
        {
            activeBeams[i] = new Beam();
            activeBeams[i] = lineRendererPrefab.instantiate(gameObject.transform);
            //lineR[i] = ((GameObject)GameObject.Instantiate(lineRendererPrefab, transform.position, transform.rotation)).GetComponent<LineRenderer>();
            //lineR[i].gameObject.transform.parent = this.transform;
        }

            maxDistVector.y = range;


		hitEffect = GetComponentInChildren<ParticleSystem>();
		if(hitEffect){
			hitEffectTransform = hitEffect.transform;
		}
        damageDealer = gameObject.GetComponent<DamageDealer>();

	}
	

	
	// Update is called once per frame
	public virtual void Update () {

		if (shootTimer > 0)
		{
			shootTimer -= Time.deltaTime;
			state = State.Waiting;
		}
		else {
			state = State.Ready;
            foreach (Beam b in activeBeams) b.disable();
            if(hitEffect){
				if(hitEffect.isPlaying){
					hitEffect.Stop();
				}
			}

		}
		
		if (reloadTimer > 0)
		{
			reloadTimer -= Time.deltaTime;
			if (currentBurst >= maxBurst)
				state = State.Reloading;

		}
		else
		{
			currentBurst = 0;
			state = State.Ready;
		}
		
		
	}
    void LateUpdate()
    {
        
        if ((state == State.Ready || state == State.Firing) && fire)
        {
            if (distr != null)
                if (distr.require(ammoType, ammoRate, true, primaryContainer) == null)
                    state = State.OutOfAmmo;
        }
            

        if (fire && shootTimer <= 0 && state == State.Ready)
        {
            state = State.Firing;
            //Execute weapon shoot script
            shootAllRays();
			
			

            currentBurst++;
            shootTimer = 1 / shotsPerSec;
            reloadTimer += shotReloadTime;
        }

    }
    protected void shootAllRays()
    {
        for (int i = 0; i < rays; i++)
        {
            activeRay = i;
            shoot();
        }
    }
    protected override void shoot()
    {

        float rotOff = Random.Range(-1 * accuracyModifier, accuracyModifier);

        Quaternion rotOffset = Quaternion.AngleAxis(rotOff, new Vector3(0, 0, 1));

        activeBeams[activeRay].enable();
        activeBeams[activeRay].SetPosition(0, spawnPoint.transform.position);

        RaycastHit2D hit;
        Quaternion oldrot = transform.rotation;
        transform.rotation *= rotOffset;
        hit = Physics2D.Raycast(spawnPoint.transform.position, transform.up, range, lMask);
        //Debug.Log("Hit:" + hit.collider+"Point "+hit.point);   
        
        if (hit.rigidbody != null)
        {
            
            //hit.rigidbody.gameObject.SendMessage("applyDamage", dmg);
            damageDealer.damageOther(hit.transform.gameObject);
            if (hitEffect)
            {
                hitEffectTransform.position = hit.point;
                if (!hitEffect.isPlaying)
                {
                    hitEffect.Play();
                }
            }
            activeBeams[activeRay].SetPosition(1, hit.point);   
        }
        else
        {
            if (hitEffect)
            {
                if (hitEffect.isPlaying)
                {
                    hitEffect.Stop();
                }
            }
            endPoint.transform.localPosition = maxDistVector;
            activeBeams[activeRay].SetPosition(1, endPoint.transform.position);
        }

        transform.rotation = oldrot;
    }

    
	public void fireMain()
	{
		//Debug.Log("Shooting! "+gameObject.GetHashCode()+"/"+gameObject.GetType());
		fire = true;    
		if (endSound != null && endSound.isPlaying) endSound.Stop();
		if (startSound != null && !startSound.isPlaying) startSound.Play();
		if (loopSound != null && !loopSound.isPlaying) loopSound.Play();
	}
	public void stopMain()
	{
		fire = false;
		if (startSound != null && startSound.isPlaying) startSound.Stop();
		if (loopSound != null && loopSound.isPlaying) loopSound.Stop();
		if (endSound != null) endSound.Play();
		
	}
	
	
	public void holdMain()
	{
		//throw new System.NotImplementedException();
	}
	
	public void fireAlt()
	{
		//throw new System.NotImplementedException();
	}
	
	public void stopAlt()
	{
		//throw new System.NotImplementedException();
	}
	
	public void holdAlt()
	{
		//throw new System.NotImplementedException();
	}
	public override string ToString()
	{
		return name + " " + this.state;
	}
    
    public float effectiveRange
    {
        get { return range * accuracy; }
    }
}