﻿using UnityEngine;
using System.Collections;

public class TrackObject : MonoBehaviour {
    // speed is the rate at which the object will rotate
    ThrusterController.turnDirection turnDir;
    public float turnTorque = 500;
    private float maxTorque;
    private float deadAngle = 0;
    public float angularVelocity;
    public float angle;
    public float reverseTurnMultiplier = 0.05f;
    public float calculatedOptimal;
    public float minOptimalAngle = 1f;
    public float torque;
    public KeyCode setTargetKey = KeyCode.Mouse0;
    GameObject guiObject;
    MouseCursor mouseCursor;
    private GameObject target;
    private bool targetIsNavpoint;
    private bool targetIsCreated = false;
    public bool trackMouse = false;
    public float angledir;
    Vector2 targetPoint;
    public LayerMask validTargetLayers;

    public bool hasTarget { get { return target != null; } }


    // Use this for initialization    
    void Start()
    {
        guiObject = GameObject.Find("GUI_Object");
        mouseCursor = guiObject.GetComponent<MouseCursor>();
        maxTorque = turnTorque;
        //add a blank navpoint if we're not tracking mouse        
        if (!trackMouse)
        {
            addTarget(createNavPoint(transform.position));
            //target.transform.position = (Vector2)transform.position;
            targetIsNavpoint = true;
            Debug.Log(targetIsCreated);
        }
    }
    void Update()
    {
        //Debug.Log(target);
            
        // Click mouse to select target. Add navpoint if no valid gameobject!
        if (Input.GetKeyDown(setTargetKey))
        {
            // = new RaycastHit();
            Vector3 position = mouseCursor.getWorldPosition();
            //GameHelper.getCamera().ScreenPointToRay(Input.mousePosition);
            //if (Physics2D.Raycast(ray, out hit,100f,validTargetLayers))
            RaycastHit2D hit = Physics2D.Raycast((Vector2)position,(Vector2)position,0f,validTargetLayers);
            if (hit.collider != null)
            {
                Debug.Log("target is a gameobject" +hit.collider);
                
                addTarget(hit.collider.gameObject);
                targetIsNavpoint = false;
                //if (hit.collider)
            }
            else
            {
                //creating navpoint
                Debug.Log("Creating Navpoint");
                addTarget(createNavPoint(position));
                targetIsNavpoint = true;
            }
            //GameObject navpoint = (GameObject) GameObject.Instantiate(Resources.Load("Prefabs/NavPoint"));
            //navpoint.transform.position = (Vector2) position;
        }
               
    }
    void LateUpdate()
    {
        //if we haven't a valid target, and we're not in the process of creating one, we want to create a random navpoint
        //if (targetIsCreated && target != null)
        //{
        //    Debug.Log("We should go here after navpoint is created " + target);
        //    targetIsCreated = false;
        //}
        //else
        //if (target == null)// && !targetIsCreated)
        //{
        //    Debug.Log("Should not be here on start " + target + " / " + targetIsCreated);
        //    addTarget(createNavPoint(transform.position, 5f));
        //    targetIsNavpoint = true;
        //}        
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if (trackMouse || target != null)
        {
            trackTarget();
        }
    }
    
    //Returns angle to target in degrees (from ship front)
    public float angleToTarget
    {
        get { return Vector2.Angle((Vector2)transform.up, targetPoint - (Vector2)transform.position); }
    }
    /*
     * This returns a negative number if B is left of A, positive if right of A, or 0 if they are perfectly aligned.
     */
    public static float AngleDir(Vector2 A, Vector2 B)
    {
        return (-A.x * B.y + A.y * B.x) * -1;
    }
    public void trackTarget()
    {
        
        angularVelocity = rigidbody2D.angularVelocity;
        
        if (trackMouse)
            targetPoint = (Vector2)mouseCursor.getWorldPosition();
        else
            targetPoint = (Vector2)target.transform.position;               

        Vector2 targetDirection = targetPoint - (Vector2)transform.position;
        Vector2 forward = (Vector2)transform.up;
        angledir = AngleDir(forward, targetDirection);        
        angle = Vector2.Angle(forward, targetDirection);
        if (angledir > 0 && angle > deadAngle)
        {
            //Debug.Log ("turning left "+angle+", "+angledir);
            turnDir = ThrusterController.turnDirection.Left;
            turnTo(angledir, angle);

            //turn left
        }
        else if (angledir < 0 && angle > deadAngle)
        {
            // turn right
            //Debug.Log("turning right " + angle + ", " + angledir);
            turnDir = ThrusterController.turnDirection.Right;
            turnTo(angledir, angle);

        }
        else turnDir = ThrusterController.turnDirection.Center;
        //Debug.Log(turnDir);
    }
    public void turnTo(float direction, float angle)
    {
        direction = Mathf.Sign(direction);
        calculatedOptimal = getOptimalAngle();
        //torque = ((angle > optimAngle) ? 1f : angle / optimAngle) * turnTorque;
        torque = ((angle > calculatedOptimal) ? 1f : angle / calculatedOptimal) * turnTorque;

        if (applyReverseTurn(angle))
        {

            //reverse thrust direction
            if (!isOppositeDir(rigidbody2D.angularVelocity, direction))
            {
                direction *= -1;
            }

            turnDir = (ThrusterController.turnDirection)((int)turnDir + 1);

            torque = ((angle > calculatedOptimal) ? 0.01f : 1 - angle / (calculatedOptimal)) * turnTorque;
            if (torque > estimatedTorqueToStop && estimatedTorqueToStop > 1f)
            {
                torque = estimatedTorqueToStop;
            }
        }

        // clamp torque
        if (torque > maxTorque) torque = maxTorque;

        rigidbody2D.AddTorque(direction * torque);
    }
    float getOptimalAngle()
    {
        float m = rigidbody2D.mass;
        float optimal = (Mathf.Pow(angularVelocity, 2f) * m) / 1000 * reverseTurnMultiplier;

        if (optimal < minOptimalAngle)
            optimal = minOptimalAngle;
        return optimal;
    }
    bool applyReverseTurn(float angle)
    {
        calculatedOptimal = getOptimalAngle();
        if (angle < calculatedOptimal)
            return true;
        else
            return false;
    }
    bool isOppositeDir(float from, float to)
    {
        //Debug.Log(Mathf.Sign(from) * Mathf.Sign(to));
        if (Mathf.Sign(from) * Mathf.Sign(to) == 1)
            return false;

        return true;
    }
    public ThrusterController.turnDirection getTurnDirection()
    {
        return turnDir;
    }
    public void addTarget(GameObject target)
    {
        Debug.Log("adding target");
        if (targetIsNavpoint && target != null)
        {
            Debug.Log("Destroying " + target);
            GameObject.Destroy(this.target);
            targetIsNavpoint = false;
        }
            
        this.target = target;
        
    }
    private GameObject createNavPoint(Vector3 position, float randomRadius = 0)
    {
        Debug.Log("Creating navpoint");
        Vector3 pos = new Vector3();
        if (randomRadius > 0)
        {
            pos = Random.insideUnitCircle * randomRadius;
        }
        GameObject navpoint = (GameObject)GameObject.Instantiate(Resources.Load("Prefabs/NavPoint"));
        navpoint.transform.position = (Vector2)(position + pos);
        return navpoint;
    }
    public void addNavPoint(Vector3 position, float randomRadius = 0)
    {
        addTarget(createNavPoint(position, randomRadius));
        targetIsNavpoint = true;
    }
    public Vector2 VectorToTarget
    {
        get
        {
            if (target != null)
                return (Vector2)(target.transform.position - transform.position);
            else return new Vector2();
        }
    }

    public float estimatedTorqueToStop
    {
        get
        {

            float r = GameHelper.getColliderRadius(collider2D);
            float estimation = rigidbody2D.angularVelocity * rigidbody2D.mass * Mathf.Pow(r,2);
            return estimation;
        }
         
        //set; 
    }
    
}
