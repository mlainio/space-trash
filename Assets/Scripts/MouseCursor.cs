﻿using UnityEngine;
using System.Collections;

public class MouseCursor : MonoBehaviour {
	public Camera theCamera;
    public Texture2D cursorImage;
    public int cursorWidth = 32;
    public int cursorHeight = 32;
    
    Vector3 currentMouseClickWorldSpace;
    Vector3 currentPosition;
    
        
	// Use this for initialization
	void Start () {
        Screen.showCursor = false;
        //Screen.lockCursor = true;
	
	}
	
	// Update is called once per frame
	void Update () {
		currentPosition = theCamera.ScreenToWorldPoint (Input.mousePosition);		
		if (Input.GetButtonDown ("Fire1")) {
			currentMouseClickWorldSpace = currentPosition;
			//Debug.Log ("Click at: "+currentMouseClickWorldSpace);
		}	
		//Debug.DrawLine (GameObject.FindGameObjectWithTag ("Player").transform.position, currentMouseClickWorldSpace, Color.green);

	}
    public Vector3 getWorldPosition()
    {
        return currentPosition;
    }    
    void OnGUI()
    {
        GUI.DrawTexture(new Rect(Input.mousePosition.x-cursorWidth*0.5f, Screen.height - Input.mousePosition.y - cursorHeight*0.5f, cursorWidth, cursorHeight), cursorImage);
    }
}
