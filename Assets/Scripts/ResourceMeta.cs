﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;


public class ResourceMeta : IResource
{
    public enum Type
    {
        Energy = 1,
        Supplies = 2,                 //Change O2 to Life Support...
        Crew = 4,
        Ordnance = 8,
        Crystal = 16,
        Mineral = 32,
        RareMineral = 64,
        ExoticMineral = 128,
        All = 256,
        RandomCargo = 512       //Randomized Crystal, Mineral, RareMineral, Exotic Mineral
    };
    //Template to store data values
    //TODO: templates should be moved somewhere else, this might not be a good place...?
    public struct ResourceTemplate
    {
        Type type;
        float massPerUnit;
        float valuePerUnit;
        public Type Type { get { return type; } }
        public float MassPerUnit { get{return massPerUnit;} }
        public float ValuePerUnit { get{return valuePerUnit;} }
        public bool ignoreMass { get; set; }
        public ResourceTemplate(Type t, float mpu, float vpu, bool nomass)
        {
            type  = t;
            massPerUnit = mpu;
            valuePerUnit = vpu;
            ignoreMass = nomass;
        }        
    }
    private static ResourceTemplate[] templates;
    //Variables
    private static bool initialized = false;
    private int quantityCount =0 ;
    public Type type;
    public bool ignoreMass = false;
    //parameters
    public int Quantity
    {
        get { return quantityCount; }
        set { quantityCount = value; }
    }

    public float Volume
    {
        get { return quantityCount * ((ignoreMass) ? 1 : ResourceMeta.templates[typeIndex(ResourceType)].MassPerUnit); }
        set { quantityCount = Mathf.RoundToInt(value * ((ignoreMass) ? 1 : ResourceMeta.templates[typeIndex(ResourceType)].MassPerUnit)); }
    }
    public Type ResourceType
    {
        get { return type; }
    }
    //Constructor
    public ResourceMeta()
    {
      
    }    
    public static int typeIndex(ResourceMeta.Type type)
    {
        //Debug.Log(type+(int)type);
        float val = (float)type;
        return (Mathf.RoundToInt(Mathf.Log(val, 2)));
    }
    public static bool typeEquals(ResourceMeta.Type type1, ResourceMeta.Type type2)
    {
        return ((type1 & type2) == type2);

    }
    public static void initResources()
    {
        if (initialized)
            return;
        // THESE SHOULD BE READ FROM SETTINGS FILE ETC.
        //Type, MassPerUnit, ValuePerUnit, IgnoreMass
        
        templates = new ResourceTemplate[typeIndex(Type.All)];
        templates[typeIndex(Type.Energy)] = new ResourceTemplate(Type.Energy, 0f, 0f, true);
        templates[typeIndex(Type.Supplies)] = new ResourceTemplate(Type.Supplies, 0.02f, 1f, false);
        templates[typeIndex(Type.Crew)] = new ResourceTemplate(Type.Crew, 0.1f, 10f, false);
        templates[typeIndex(Type.Ordnance)] = new ResourceTemplate(Type.Ordnance, 0.010f, 0.1f, false);
        templates[typeIndex(Type.Crystal)] = new ResourceTemplate(Type.Crystal, 0.20f, 3.0f, false);
        templates[typeIndex(Type.Mineral)] = new ResourceTemplate(Type.Mineral, 0.1f, 1f, false);
        templates[typeIndex(Type.RareMineral)] = new ResourceTemplate(Type.RareMineral, 0.15f, 5f, false);
        templates[typeIndex(Type.ExoticMineral)] = new ResourceTemplate(Type.ExoticMineral, 0.25f, 20f, false);
    }

    public IResource createFromTemplate(ResourceMeta.ResourceTemplate template, float volume)
    {
        ResourceMeta newres = new ResourceMeta();
        newres.type = template.Type;
        //Debug.Log(template.Type);
        newres.Volume = volume;
        newres.ignoreMass = template.ignoreMass;
        return newres;
        //throw new System.NotImplementedException();
    }

    public static ResourceTemplate getTemplate(Type resourcetype)
    {
        //Debug.Log(templates.Length+" vs "+typeIndex(resourcetype));
        return templates[typeIndex(resourcetype)];
    }
    public static int volumeToQuantity(ResourceMeta.Type resource, float volume)
    {
        ResourceTemplate temp = getTemplate(resource);
        return ((temp.ignoreMass) ? Mathf.RoundToInt(volume) : Mathf.FloorToInt(volume / temp.MassPerUnit));
    }
    public override string ToString()
    {
        return ResourceType + ": " + "x" + Quantity + " units "+((ignoreMass) ? "" : (Volume + " tons"));
    }
}


