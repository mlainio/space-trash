﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class ResourcePhysical : MonoBehaviour, IResource, ITradeable {
    /*
     * TODO: Create some resource Prefabs....
     */

    public ResourceMeta.Type type;
    public string ResourceName;    
    //public float valuePerUnit = 1f;     // TODO: should be a private value read from prototype specs...
    //public bool quantityAsCapacity = false;
    //public float massPerUnit = 1f;    // TODO: should be a private value read from prototype specs...
    public int forceUnitCountTo = 0;        // change object mass according to desired number of units.
	// Use this for initialization
	void Start () {
       if (forceUnitCountTo >= 1)
            Quantity = forceUnitCountTo;                	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public int Quantity
    {
        get { return Mathf.RoundToInt(rigidbody2D.mass / ResourceMeta.getTemplate(type).MassPerUnit); }
        set { rigidbody2D.mass = value * ResourceMeta.getTemplate(type).MassPerUnit; }
    }
    public float Volume
    {
        get { //Debug.Log("At Volume! g");
                return rigidbody2D.mass; }
        set { //Debug.Log("At Volume! s");
            rigidbody2D.mass = value; }
    }
    public ResourceMeta.Type ResourceType
    {
        get { return type; }
    }
    public IResource createFromTemplate(ResourceMeta.ResourceTemplate template, float volume)
    {
        ResourceMeta newres = new ResourceMeta();
        newres.type = template.Type;
        Debug.Log(template.Type);
        newres.Volume = volume;
        newres.ignoreMass = template.ignoreMass;
        return newres;        
    }

    public float Value
    {
        get { throw new System.NotImplementedException(); }
    }
    public override string ToString()
    {
        return ResourceType + ": " + "x" + Quantity + " / " + Volume + " tons";
    }
}
