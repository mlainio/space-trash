﻿using UnityEngine;
using System.Collections;


public class GravityWell : MonoBehaviour {

    public static double G = 6.67428e-5;
    public double mass;
    public float deadRadius = 2;
    public float gravForce = 200f; //Pull > 0 
    public float forceCurve = 1.6f; // higher -> steeper curve, lower -> more linear (Affects actual force!) MIN 0
    public bool useRealGravity = true;
    
	// Use this for initialization
	void Start () {
      
	}
	
    void OnTriggerStay2D(Collider2D other)
    {
        Rigidbody2D rb;
        try
        {
            if (other.gameObject.rigidbody2D == null)
                rb = other.transform.parent.rigidbody2D; 
            else
                rb = other.gameObject.rigidbody2D;
            if (other.gameObject.tag == "IgnorePhysics")
                return;
            //Debug.Log(gameObject.GetHashCode() + " gravity well to" + other.gameObject.GetHashCode() );
            Vector2 gravVector = (Vector2)(other.transform.position - transform.position);
            Vector2 force = -gravVector.normalized * getGravityForce(mass, rb.mass, gravVector.magnitude, useRealGravity, deadRadius, gravForce, forceCurve);
            rb.AddForce(force);
        }
        catch (System.NullReferenceException e)
        {
            
        }        

    }
    public static float getGravityForce(double firstMass, double secondMass, float distance, bool realGravity, float deadRadius = 0f, float fakeForce = 0f, float fakeCurve = 0f)
    {        
        float gravity = 0f;

        if (realGravity)
        {
            if (distance > deadRadius)
                gravity = (float)(G * (firstMass * secondMass / Mathf.Pow(distance, 2)));
        }
        else
        {
            if (distance > deadRadius)
            {
                gravity = fakeForce / Mathf.Pow(distance, fakeCurve);
            }
        }
        //Debug.Log(gravity);
        return gravity;
    }

}
