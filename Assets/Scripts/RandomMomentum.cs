﻿using UnityEngine;
using System.Collections;

public class RandomMomentum : MonoBehaviour {
    public Vector2 forceVector;
    public float torque;
    public bool random=true;
    public bool start;
    public bool constantForce = false;
    public float randomForceMultiplier = 5f;
    private float constantForceFrequency =1f;
    private float timer;
    
	// Use this for initialization
	void Start () {
             
    }

    public void applyMomentum()
    {
        rigidbody2D.AddForce(forceVector);
        rigidbody2D.AddTorque(torque);
        start = false;
    }
    public void applyRandomizedMomentum(float randomForceMultiplier)
    {
        this.randomForceMultiplier = randomForceMultiplier;
        randomize();
        start = true;
    }
	
	// Update is called once per frame
	void Update () {

        if (start)
        {
            //Debug.Log("Random Momentum!");
            if (random)
            {
                randomize();
                //random = false;
            }

            applyMomentum();
            
            
        }
        if (constantForce)
        {
            if (timer >= constantForceFrequency)
            {
                applyMomentum();
                timer = 0;
            }
                
            timer += Time.deltaTime;
        }

            
        
	}
    public void randomize()
    {
        float power = Random.Range(0.25f, 1.0f);
        forceVector = power * (Vector2)Random.onUnitSphere * Mathf.Pow(rigidbody2D.mass,2f) * randomForceMultiplier;
        torque = Mathf.Pow(rigidbody2D.mass,1.2f) * Random.Range(-1f, 1f) * randomForceMultiplier;
    }
}
