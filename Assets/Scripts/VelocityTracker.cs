﻿/*
* VelocityTracker keeps track of the gameobjects velocity for last n frames
* It stores current velocity, but average velocity is based on last n frames, not current frame
*/

using UnityEngine;
using System.Collections;

//[RequireComponent (typeof(Rigidbody2D))]
public class VelocityTracker : MonoBehaviour
{    
    //public bool enabled = true;
    public int totalFrames = 5;
    private int index;
    private Vector2[] tracker;
    private Vector2 newVelocity;
    private Vector3 lastPosition;
    public Vector2 velocityNow;
    public Vector2 fauxVelocityNow;
    private bool useRigidBody
    {
        get {
            if (gameObject.rigidbody2D != null)
            {
                if (gameObject.rigidbody2D.isKinematic)
                    return false;
                else return true;
            }
            return false;

        }
    }
    
    public void Start()
    {
        index = 0;
        tracker = new Vector2[totalFrames];
        for (int i = 0; i < totalFrames; i++)
        {
            tracker[i] = new Vector2();
        }
        lastPosition = transform.position;
    }
    void FixedUpdate()
    {        
        if (enabled)
        {
            if (useRigidBody)
                addFrame(rigidbody2D.velocity);
            else
                addFrame(fauxVelocity);

        }
        velocityNow = velocity;
        fauxVelocityNow = fauxVelocity;
        //Debug.Log(hasRigidBody);
        lastPosition = transform.position;
    }
    public Vector2 velocity 
    {
        get {
            if (useRigidBody)
                return rigidbody2D.velocity;
            else
                return fauxVelocity;
        }
    }

    private Vector2 fauxVelocity
    {
        get { return (Vector2)(transform.position - lastPosition)*50f; }
    }
    public void addFrame(Vector2 velocity)
    {
        if (index >= totalFrames) index = 0;
        //we're only interested in the last n frames, not current frame...
        tracker[index] = newVelocity;
        newVelocity = velocity;
        index++;
    }
    public Vector2 averageVelocity()
    {
        Vector2 vel = new Vector2();
        int j = 0;
        while (j < totalFrames)
        {
            vel += tracker[j];
            j++;
        }
        vel = vel / totalFrames;
        return vel;
    }
}
