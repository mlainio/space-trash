﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
    public WeaponController controller;
    public enum State { Ready, Firing, Waiting, Reloading, Targeting, OutOfAmmo, Disabled}
    public float accuracy = 0.95f;
    public float maxShootAngle = 10f;
    protected float accuracyModifier;
    protected ResourceDistributor distr;
    protected State state
    {
        get;
        set;
    }
    
    protected Rigidbody2D parentBody;
	
    // Use this for initialization
    public virtual void Start()
    {
        state = State.Ready;
        parentBody = controller.parentBody;
        distr = parentBody.GetComponentInChildren<ResourceDistributor>();
        accuracyModifier = (maxShootAngle - maxShootAngle * accuracy) / 2;
    }

    virtual protected void shoot()
    {
        Debug.Log("Stub shoot method called!");
    }
    
    public override string ToString()
    {
        return name + " " + this.state;
    }    
}
