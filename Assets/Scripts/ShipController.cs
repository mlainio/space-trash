﻿using UnityEngine;
using System.Collections;

public abstract class ShipController : MonoBehaviour {
    //public float turnSpeed = 1.0f;
    public float moveForce = 1.0f;
    public float reverseMultiplier = 0.55f;
    public float turboMultiplier = 2.0f;
    public float strafeMultiplier = 0.60f;
    public float auxThrusterMultiplier = 0.25f;
    protected Vector2 forceVector = new Vector2();
    Vector2 direction = new Vector2();
    public float maxVelocity = 20f;
    public float maxTurboVelocity = 75f;
    protected int fuelConsumption = 5;
    protected int turboConsumption = 20;

    protected ResourceDistributor distributor;

    protected enum MoveState { Normal, Turbo, TurboSlowdown, ManualSlowdown };
    protected enum ThrusterMode { Normal, Aux, Inactive };
    protected ThrusterMode mode;
    protected MoveState turbo;
    protected float currentSpeed;
    protected float accelerate;
    protected float strafe;
    protected ThrusterController thrustControl;
    protected VelocityTracker velocityTracker;
    // Use this for initialization
    public virtual void Start()
    {
        distributor = gameObject.GetComponentInChildren<ResourceDistributor>();
        thrustControl = GetComponentInChildren<ThrusterController>();
        velocityTracker = gameObject.GetComponent<VelocityTracker>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        // get input from controller and determine fuel consumption
        int requireFuel = getMoveInput();

        //If fuel check passes apply forcevector based on acceleration and strafe values
        if (fuelCheck(requireFuel))
        {
            //Debug.Log("FuelCheck Passed!");
            mode = ThrusterMode.Normal;
            applyMovement(getForceVector());
        }
        else
        {
            mode = ThrusterMode.Aux;
            if (turbo == MoveState.Turbo)
                accelerate /= turboMultiplier;
            applyMovement(getForceVector() * auxThrusterMultiplier);

        }


    }

    void LateUpdate()
    {
        //Draw up direction
        //Debug.DrawLine (transform.position, transform.position+transform.up*255);
        //Draw Move vector
        Debug.DrawLine(transform.position, transform.position + new Vector3(rigidbody2D.velocity.x, rigidbody2D.velocity.y), Color.blue);
        //Draw Acceleration (force) vector
        Debug.DrawLine(transform.position, transform.position + new Vector3(getForceVector().x, getForceVector().y), Color.red);        
        //Debug.DrawLine(transform.position, transform.position+transform.up * 10f);
    }
    // Apply movement based on forcevector
    protected void applyMovement(Vector2 forceVector)
    {
        if (forceVector.magnitude == 0)
            mode = ThrusterMode.Inactive;
        // predict speed and control it
        Vector2 predictMove = forceVector / rigidbody2D.mass + rigidbody2D.velocity;

        if ((predictMove.magnitude > maxVelocity && turbo == MoveState.Normal || predictMove.magnitude > maxTurboVelocity && turbo == MoveState.Turbo) && mode != ThrusterMode.Inactive)
        {
            //Debug.Log("clamping required");
            rigidbody2D.velocity = (Vector2)Vector3.ClampMagnitude((Vector3)rigidbody2D.velocity, ((turbo == MoveState.Turbo) ? maxTurboVelocity : maxVelocity));
            //  **turn main engines off**
            //if (thrustControl != null)
            //{
            //    thrustControl.BroadcastMessage("Halt", Thruster.Position.Main, SendMessageOptions.RequireReceiver);
            //    thrustControl.BroadcastMessage("Halt", Thruster.Position.Turbo);
            //}
        }
        else if (turbo == MoveState.TurboSlowdown || turbo == MoveState.ManualSlowdown)
        {
            //thrustControl.BroadcastMessage("Release", SendMessageOptions.RequireReceiver);
            if (currentSpeed > maxVelocity || turbo == MoveState.ManualSlowdown)
            {
                //fixed slowdown speed at opposite direction
                forceVector = forceVector - (Vector2)(rigidbody2D.velocity.normalized * reverseMultiplier * moveForce);

                // negate any acceleration
                if (accelerate > 0)
                {
                    forceVector -= (Vector2)(rigidbody2D.transform.up * accelerate * moveForce);
                }
            }
            else turbo = MoveState.Normal;

        }
        else if (thrustControl != null)
        {
            thrustControl.BroadcastMessage("Release", SendMessageOptions.RequireReceiver);

        }

        rigidbody2D.AddForce(forceVector);
    }
    protected bool fuelCheck(int resourceQuantity)
    {
        if (distributor == null)
            return true;

        return (distributor.require(ResourceMeta.Type.Energy, resourceQuantity, true) != null);

    }
    // Returns required fuel
    //
    protected abstract int getMoveInput();
    
    //Gets the forcevector based on acceleration and strafe speed;
    public virtual Vector2 getForceVector()
    {
        direction = rigidbody2D.transform.up;
        forceVector = direction * accelerate * moveForce * ((accelerate < 0) ? reverseMultiplier : 1);

        Vector2 strafeVector = rigidbody2D.transform.right * strafe * moveForce * strafeMultiplier;
        forceVector = forceVector + strafeVector;

        return forceVector;
    }
    public Vector2 getLocalForceVector()
    {
        //return new Vector2(strafe * moveForce * strafeMultiplier, accelerate * moveForce);
        //Debug.Log("ForceVector: "+ forceVector);
        return (Vector2)transform.InverseTransformDirection(forceVector) * forceVector.magnitude;
    }

    public Vector2 getMoveVector()
    {
        return rigidbody2D.velocity;
    }
    public bool isTurbo()
    {
        return (turbo == MoveState.Turbo && mode == ThrusterMode.Normal);
    }
    public bool slowdown()
    {
        return (turbo == MoveState.TurboSlowdown || turbo == MoveState.ManualSlowdown);
    }
    public bool isAuxMode()
    {
        return (mode == ThrusterMode.Aux);
    }
}
