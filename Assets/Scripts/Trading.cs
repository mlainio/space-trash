﻿using UnityEngine;
using System.Collections;

public class Trading : MonoBehaviour {

	private ResourceDistributor rDistributor;
	private ResourceMeta rMeta;
	private GameObject player;
	private float credits = 0f;

	public float mineraValue = 10f;
	public float rareValue = 75f;
	public float exoticValue = 200f;

	public ResourceContainer rContainer;
	public ResourceMeta.Type mineral;
	public ResourceMeta.Type rareMineral;
	public ResourceMeta.Type exoticMineral;

	private ResourceMeta traded;
	private bool drawNotification = false;
	private bool canTrade = false;
	Camera mainCam;
	GameMenu gm;


	// Use this for initialization
	void Start () {
		player = GameHelper.getPlayer();
		rDistributor = player.GetComponentInChildren<ResourceDistributor>();
		rContainer = player.GetComponentInChildren<ResourceContainer>();
		mainCam = GameHelper.getCamera();
		gm = mainCam.GetComponentInChildren<GameMenu>();

		//ResourceMeta.getTemplate(mineral);

	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.R) && canTrade == true){
			if(gm.getCustomizeMenu() == false){
				Time.timeScale = 0;
				gm.setCustomizeMenu(true);
				mainCam.GetComponentInChildren<PlayerHUD>().setGUI(false);
			}
			else if(gm.getCustomizeMenu() == true){
				Time.timeScale = 1;
				gm.setCustomizeMenu(false);
				mainCam.GetComponentInChildren<PlayerHUD>().setGUI(true);
			}
		}
	}

	void OnTriggerStay2D(Collider2D other){
		if(other.gameObject.tag == "Starbase"){
			drawNotification = true;
			if(Input.GetKey(KeyCode.E)){
				traded=rDistributor.require(mineral,100,true,rContainer);
				if(traded != null){
					credits+=traded.Quantity*mineraValue;
				}
				traded=rDistributor.require(rareMineral,100,true,rContainer);
				if(traded != null){
					credits+=traded.Quantity*rareValue;
				}
				traded=rDistributor.require(exoticMineral,100,true,rContainer);
				if(traded != null){
					credits+=traded.Quantity*exoticValue;
				}
			}

			//Debug.Log("In trade zone");
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		if(other.gameObject.tag == "Starbase"){
			canTrade = true;
		}
	}


	void OnTriggerExit2D(Collider2D other){
		if(other.gameObject.tag == "Starbase"){
			drawNotification = false;
			canTrade = false;
		}
	}

	void OnGUI (){
		if(drawNotification == true){
			GUI.Label(new Rect(Screen.width/2,Screen.height/2,200,50),"Press E to sell resources, R to open customization menu");
		}
		//GUI.Label(new Rect(Screen.width/2+200,Screen.height/2,100,40),credits.ToString());
	}

	public float getCredits(){
		return credits;
	}

	public void deductCredits(float f){
		if(credits - f >= 0){
			credits = credits - f;
		}
		else{
			Debug.Log("Credits can't be negative");
		}
	}

	public void setDrawNotification(bool b){
		drawNotification = b;
	}
	/*
	if(gm.getDraw()==false && gm.getCustomizeMenu()==false){
		gm.setDraw(true);
		Time.timeScale = 0;
		mainCam.GetComponentInChildren<PlayerHUD>().setGUI(false);
	}
	else if(gm.getDraw()==false && gm.getCustomizeMenu()==true){
		gm.setCustomizeMenu(false);
		gm.setDraw(true);
	}
	
	else if(gm.getDraw()==true){
		gm.setDraw(false);
		mainCam.GetComponentInChildren<PlayerHUD>().setGUI(true);
		Time.timeScale = 1;
	}
	*/
}
