﻿using UnityEngine;
using System.Collections;

public class PlayerShipController : ShipController {
    //public bool homo;
    void Update()
    {
        if (Input.GetKey(KeyCode.LeftShift))
            turbo = MoveState.Turbo;
        else if (Input.GetKeyUp(KeyCode.LeftShift))
            turbo = MoveState.TurboSlowdown;
        else if (Input.GetKey(KeyCode.Space))
            turbo = MoveState.ManualSlowdown;
        else if (Input.GetKeyUp(KeyCode.Space))
            turbo = MoveState.Normal;
    }

    protected override int getMoveInput()
    {
        //Debug.Log("Player Move Input!"+Time.deltaTime);
        int requireFuel = 0;
        currentSpeed = rigidbody2D.velocity.magnitude;
        accelerate = Input.GetAxis("Vertical");

        if (turbo == MoveState.ManualSlowdown || turbo == MoveState.TurboSlowdown)
            requireFuel = Mathf.RoundToInt(fuelConsumption * 1.25f);
        else
        {
            if (accelerate > 0)
            {
                if (turbo == MoveState.Turbo)
                {
                    accelerate *= turboMultiplier;
                    requireFuel = turboConsumption;
                }
                else
                    requireFuel = fuelConsumption;
            }
            else if (accelerate < 0)
                requireFuel = Mathf.RoundToInt(fuelConsumption * reverseMultiplier);

            strafe = Input.GetAxis("Horizontal");
            if (strafe != 0)
                requireFuel += Mathf.RoundToInt(fuelConsumption * strafeMultiplier);
        }
        return requireFuel;
    }

    ////public float turnSpeed = 1.0f;
    //public float moveSpeed = 1.0f;
    //public float reverseMultiplier = 0.55f;
    //public float turboMultiplier = 2.0f;
    //public float strafeMultiplier = 0.60f;
    //public float auxThrusterMultiplier = 0.25f;
    //Vector2 forceVector = new Vector2();
    //Vector2 direction = new Vector2();
    //public float maxSpeed = 20f;
    //public float maxTurboSpeed = 75f;
    //private int fuelConsumption = 5;
    //private int turboConsumption = 20;
    
    //private ResourceDistributor distributor;

    //enum MoveState {Normal, Turbo, TurboSlowdown, ManualSlowdown};
    //enum ThrusterMode { Normal, Aux, Inactive };
    //ThrusterMode mode;
    //MoveState turbo;
    //float currentSpeed;
    //float accelerate;
    //float strafe;
    //ThrusterController thrustControl;
	// Use this for initialization
    //void Start () {
    //    distributor = gameObject.GetComponentInChildren<ResourceDistributor>();
    //    thrustControl = GetComponentInChildren<ThrusterController>();
    //}
	
	// Update is called once per frame
    //void FixedUpdate () {

    //    int requireFuel = getMoveInput();
    //    if (fuelCheck(requireFuel))
    //    {
    //        mode = ThrusterMode.Normal;
    //        applyMovement(getForceVector());
    //    }
    //    else
    //    {
    //        mode = ThrusterMode.Aux;
    //        if (turbo == MoveState.Turbo)
    //            accelerate /= turboMultiplier;
    //        applyMovement(getForceVector() * auxThrusterMultiplier);
            
    //    }
            

    //} 

    //void LateUpdate() {
    //    //Draw up direction
    //    //Debug.DrawLine (transform.position, transform.position+transform.up*255);
    //    //Draw Move vector
    //    Debug.DrawLine (transform.position, transform.position+new Vector3(rigidbody2D.velocity.x, rigidbody2D.velocity.y), Color.blue);
    //    //Draw Acceleration (force) vector
    //    Debug.DrawLine (transform.position,transform.position+new Vector3(forceVector.x, forceVector.y),Color.red);
    //}
    //private void applyMovement(Vector2 forceVector)
    //{
    //    if (forceVector.magnitude == 0)
    //        mode = ThrusterMode.Inactive;
    //    // predict speed and control it
    //    Vector2 predictMove = forceVector / rigidbody2D.mass + rigidbody2D.velocity;

    //    if ((predictMove.magnitude > maxSpeed && turbo == MoveState.Normal || predictMove.magnitude > maxTurboSpeed && turbo == MoveState.Turbo) && mode != ThrusterMode.Inactive)
    //    {
    //        //Debug.Log("clamping required");
    //        rigidbody2D.velocity = (Vector2)Vector3.ClampMagnitude((Vector3)rigidbody2D.velocity, ((turbo == MoveState.Turbo) ? maxTurboSpeed : maxSpeed));
    //        //turn main engines off
    //        if (thrustControl != null)
    //        {
    //            thrustControl.BroadcastMessage("Halt", Thruster.Position.Main, SendMessageOptions.RequireReceiver);
    //            thrustControl.BroadcastMessage("Halt", Thruster.Position.Turbo);
    //        }
    //    }
    //    else if (turbo == MoveState.TurboSlowdown || turbo == MoveState.ManualSlowdown)
    //    {
    //        //thrustControl.BroadcastMessage("Release", SendMessageOptions.RequireReceiver);
    //        if (currentSpeed > maxSpeed || turbo == MoveState.ManualSlowdown)
    //        {
    //            //fixed slowdown speed at opposite direction
    //            forceVector = forceVector - (Vector2)(rigidbody2D.velocity.normalized * reverseMultiplier * moveSpeed);

    //            // negate any acceleration
    //            if (accelerate > 0)
    //            {
    //                forceVector -= (Vector2)(rigidbody2D.transform.up * accelerate * moveSpeed);
    //            }
    //        }
    //        else turbo = MoveState.Normal;

    //    }
    //    else if (thrustControl != null)
    //    {
    //        thrustControl.BroadcastMessage("Release", SendMessageOptions.RequireReceiver);

    //    }

    //    rigidbody2D.AddForce(forceVector);
    //}
    //private bool fuelCheck(int resourceQuantity)
    //{
    //    if (distributor == null)
    //        return true;
        
    //    return (distributor.require(ResourceMeta.Type.Energy, resourceQuantity, true) != null);
        
    //}

   
    //public Vector2 getForceVector()
    //{
    //    direction = rigidbody2D.transform.up;
    //    forceVector = direction * accelerate * moveSpeed * ((accelerate < 0) ? reverseMultiplier : 1);

    //    Vector2 vector2 = rigidbody2D.transform.right * strafe * moveSpeed * strafeMultiplier;
    //    forceVector = forceVector + vector2;
        
    //    return forceVector;
    //}
    //public Vector2 getLocalForceVector()
    //{
    //    return new Vector2(strafe * moveSpeed*strafeMultiplier, accelerate * moveSpeed);
    //}

    //public Vector2 getMoveVector()
    //{
    //    return rigidbody2D.velocity;
    //}
    //public bool isTurbo()
    //{
    //    return (turbo == MoveState.Turbo && mode == ThrusterMode.Normal);
    //}
    //public bool slowdown()
    //{
    //    return (turbo == MoveState.TurboSlowdown || turbo == MoveState.ManualSlowdown);
    //}
    //public bool isAuxMode()
    //{
    //    return (mode == ThrusterMode.Aux);
    //}
}
