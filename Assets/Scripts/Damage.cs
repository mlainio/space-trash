﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Damage {
    public enum Type
    {
        Kinetic,
        Heat,
        Energy,
        Electric,
        Gravity,
        Ballistic
    };

    public Type type;
    public float value;
    public float penetration;
    public UnityEngine.Vector2 effectVector;

    public Damage(Type type, float value, float penetration = 0, UnityEngine.Vector2 effect = new UnityEngine.Vector2())
    {
        this.type = type;
        this.value = value;
        this.penetration = penetration;
        effectVector = effect;
    }
    public float get()
    {
        return value;
    }
    public static float kinetic(UnityEngine.Vector2 velocity, UnityEngine.Vector2 otherVelocity, float mass, float otherMass, Damage damage)
    {
        //baseDamage = Mathf.Pow((gameObject.rigidbody2D.velocity - other.gameObject.rigidbody2D.velocity).magnitude,2) * 0.5f;
        //float dmg = UnityEngine.Mathf.Pow((otherVelocity - velocity).magnitude, 2) * 0.01f;        
        float dmg = (otherVelocity - velocity).magnitude * 0.10f;
        
        //float sizeMultiplier = (mass + damage.penetration) / otherMass;
        //float sizeMultiplier = mass * otherMass *0.01f;
        float sizeMultiplier = (mass + otherMass) * 0.10f;

        string damageLog = "Damage Calculations: ";
        damageLog += " Velocity " + dmg+ " * ";
        damageLog += " Size Multiplier " + sizeMultiplier + " * Base Damage " + damage.value;

        //tempDamage = Mathf.Round(Mathf.Sqrt(tempDamage * resistanceMultiplier * damage.value));
        dmg = dmg * sizeMultiplier * damage.value;
        
        //Set minimum damage
        if (dmg < 1) dmg = 1f;

        damageLog += " = " + dmg + " damage";
        Debug.Log(damageLog);
        return dmg;
    }
    //Generic damage: Ignores physical properties, slight randomization
    public static float basic(Damage damage)
    {
        float variance = 0.35f;
        float dmg = Random.Range(1-variance, 1+variance);
        string damageLog = "Damage Calculations: ";
        damageLog += "Base Damage " + damage.value;
        dmg = dmg * damage.value;
        //Set minimum damage
        if (dmg < 1) dmg = 1f;
        damageLog += " = " + dmg + " damage";
        return dmg;
    }
}
