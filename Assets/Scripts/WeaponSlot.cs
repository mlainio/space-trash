using UnityEngine;
using System.Collections;

public class WeaponSlot : MonoBehaviour {

    public GameObject assignedWeapon;
    WeaponController controller;
    public int assignedGroup;
    GameObject child;

    // Use this for initialization
	void Awake () {
        Debug.Log("WeaponSlot");
        controller = transform.parent.gameObject.GetComponent<WeaponController>();
        if (assignedWeapon == null)
        {
            //use default weapon
            assignedWeapon = controller.defaultWeapon;
        }        
        
        
	}
    public void activate()
    {
        //create a weapon
        child = controller.createWeapon(assignedWeapon, transform, controller);
        child.renderer.sortingOrder = gameObject.renderer.sortingOrder;
        controller.addWeaponToGroup((IShootable) child.GetComponent<Weapon>(), assignedGroup);
        gameObject.SendMessageUpwards("fetchContainers",SendMessageOptions.DontRequireReceiver);        
    }
	public void activateUpdate(int group)
	{
		//create a weapon
		child = controller.createWeapon(assignedWeapon, transform, controller);
		child.renderer.sortingOrder = gameObject.renderer.sortingOrder;
		controller.UpdateNewWeaponToGroup((IShootable) child.GetComponent<Weapon>(), group);
        gameObject.SendMessageUpwards("fetchContainers",SendMessageOptions.DontRequireReceiver);
	}

	public void setWeapon(GameObject wpn,GameObject go)
	{
		assignedWeapon = wpn;
		child=go;
		print("setWeapon called:"+assignedWeapon);
	}
	public GameObject getAssignedWeapon()
	{
		return assignedWeapon;
	}
	public int getAssignedGroup()
	{
		return assignedGroup;
	}
	public void setAssignedGroup(int i)
	{
		assignedGroup = i;
	}
	
	// Update is called once per frame
	void Update () {        
	
	}
}
