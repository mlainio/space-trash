﻿using System;
using UnityEngine;

[System.Serializable]
public class Armor
{
    public float mass {
        get { return rating * 0.0002f; }
    }
    public Damage.Type type;
    public float rating;              //how thick of an armor we're talking about
    public float effectiveness;    // 0 >= absorption <= 1      determines how many points of damage a single point of armor will negate
    public float reflection;       // Amount of damage reduced  Removes damage and reflects it away without damaging armor. NOT USED!

    public Armor()
    {
        type = Damage.Type.Kinetic;
        rating = 0;
        effectiveness = 0;
        reflection = 0;
    }
    //old function
    public Damage processDamage1(Damage damage)
    {
        // IF damage type is equal, reduce damage accordingly
        if (type == damage.type)
        {
            damage.value = (damage.value <= (reflection-damage.penetration) ? 0 : damage.value - ((damage.penetration >= reflection) ? 0 : (reflection-damage.penetration)));
            if (damage.value > 0)
            {
                int absorbed = Mathf.FloorToInt(damage.value * effectiveness);
                if (rating >= absorbed)
                {
                    rating -= absorbed;
                    damage.value -= absorbed;
                }
                else
                {
                    damage.value -= rating;
                    rating = 0;
                }
            }
        }

        return damage;
    }
    //old function
    public Damage processDamage2(Damage damage)
    {
        // IF damage type is equal, reduce damage accordingly
        if (type == damage.type)
        {   
            // Reflection reduces some damage right off
            damage.value = ((damage.value <= (reflection - damage.penetration)) ? 0 : damage.value - ((damage.penetration >= reflection) ? 0 : (reflection - damage.penetration)));
            
            // Absorbption determines how much damage absorbed right off by armor, damagin it in the process
            // the rest of the damage goes to hull...
            if (damage.value > 0)
            {
                int absorbed = Mathf.FloorToInt(damage.value * effectiveness);
                if (rating >= absorbed)
                {
                    rating -= absorbed;
                    damage.value -= absorbed;
                }
                else
                {
                    damage.value -= rating;
                    rating = 0;
                }
            }
            if (rating > 0)
                damage.value = damage.value * (damage.value + damage.penetration) / (rating + damage.value);
            
        }
        
        return (damage);
    }
    public Damage processDamage3(Damage damage)
    {
        // IF damage type is equal, reduce damage accordingly
        if (type == damage.type)
        {
            // Reflection reduces some damage right off
            //damage.value = ((damage.value <= (reflection - damage.penetration)) ? 0 : damage.value - ((damage.penetration >= reflection) ? 0 : (reflection - damage.penetration)));
            damage.value -= reflection;

            float leak = damage.value * damage.penetration;
            Debug.Log(damage.value+" "+damage.penetration +"->"+ leak);
            float adjustedDamage = damage.value - leak;
            

            if (damage.value > 0)
            {
                Debug.Log(adjustedDamage+" vs "+rating+ " "+effectiveness);
                if (rating > 0)
                {
                    float requiredArmor = adjustedDamage / effectiveness;

                    //we dont have enough armor, so well spend whatever we have!
                    if (requiredArmor >= rating)
                    {
                        adjustedDamage -= rating * effectiveness;
                        rating = 0;
                    }
                    else
                    {
                        adjustedDamage = 0;
                        rating -= Mathf.Round(requiredArmor * 100f) / 100f;
                    }
                }
            }
            damage.value = Mathf.Round((adjustedDamage + leak) * 100f)/100f;
            Debug.Log(damage.value);
        }

        return (damage);
    }
    public Damage processDamage(Damage damage)
    {
        // IF damage type is equal, reduce damage accordingly
        if (type == damage.type)
        {
            // Reflection reduces some damage right off
            //damage.value = ((damage.value <= (reflection - damage.penetration)) ? 0 : damage.value - ((damage.penetration >= reflection) ? 0 : (reflection - damage.penetration)));
            damage.value -= reflection;                         //damage is reduced right off by reflection
            float leak = damage.value * damage.penetration;     // some damage "leaks through" according to penetration
            float adjustedDamage = damage.value - leak;         // total damage value is still the same, but leaked damage is not handled by armor

            if (damage.value > 0)            {

                float damageMultiplier = adjustedDamage / (adjustedDamage + rating);
                float requiredArmor = (adjustedDamage - damageMultiplier * adjustedDamage) / effectiveness;
                adjustedDamage = damageMultiplier * adjustedDamage;
                
                //we dont have enough armor, so well spend whatever we have!
                if (requiredArmor >= rating)
                {
                    rating = 0;
                }
                else
                {
                    rating -= Mathf.Round(requiredArmor * 100f) / 100f;
                }
            }
            damage.value = Mathf.Round((adjustedDamage + leak) * 100f) / 100f;
            Debug.Log("Damage left after armor: "+damage.value);
        }

        return (damage);
    }

    public override string ToString()
    {
        return type + " / "+rating+" pts / " + effectiveness + " / " + reflection;
    }

}