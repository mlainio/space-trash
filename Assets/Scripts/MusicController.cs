﻿using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {
    
    public enum MusicState { Action, Exploration}
    
    [System.Serializable]
    public class Song
    {
        public MusicState state;
        public AudioClip clip;
        AudioSource source;

        public void setSource(GameObject parentobject)
        {
            source = parentobject.AddComponent<AudioSource>();
            source.clip = clip;
            //source.
            source.loop = true;
            source.playOnAwake = false;
        }
        public void stop()
        {
            source.Stop();
        }
        public void play()
        {
            source.Play();
        }
        public float volume
        {
            get { return source.volume; }
            set { source.volume = value; }
        }
    }
    public KeyCode nextSong = KeyCode.N;
    public KeyCode mute = KeyCode.M;
    public bool muted = false;
    [SerializeField]
    public Song[] playlist;
    public float volume = 0.65f;    
    public float fadeTime = 2.0f;
    private int currentSong = 0;
    MusicState currentState = MusicState.Exploration;
    ObjectScanner scanner;

	// Use this for initialization
	void Start () {
	    foreach (Song s in playlist)
        {
            s.setSource(this.gameObject);
        }
        playlist[currentSong].volume = this.volume;
        playlist[currentSong].play();
        scanner = GameHelper.getPlayer().GetComponent<ObjectScanner>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(nextSong))
        {
            playNextSong();
        }
        if (Input.GetKeyDown(mute))
        {
            muteVolume();
        }

        if (scanner != null)
        {
            if (scanner.getNearestObjectWithTag("Enemy") != null)
            {
                currentState = MusicState.Action;
            }
            else
                currentState = MusicState.Exploration;
        }
        
        while (playlist[currentSong].state != currentState)
        {
            playNextSong();
        }
	}

    private void muteVolume()
    {        
        foreach (Song s in playlist)
        {
            if (!muted)
                s.volume = 0f;
            else
                s.volume = volume;
        }
        muted = !muted;
    }

    private void playNextSong()
    {
        int oldsong = currentSong;
        if (currentSong + 1 >= playlist.Length)
        {
            currentSong = 0;
        }
        else
        {
            currentSong++;
        }
        StartCoroutine(crossFade(playlist[oldsong], playlist[currentSong]));
        
    }
    public IEnumerator crossFade(Song oldSong, Song newSong)
    {
        float timer = fadeTime;
        newSong.volume = 0;
        newSong.play();
        while (timer > 0)
        {
            timer -= Time.deltaTime;
            float volumeStep = Time.deltaTime / fadeTime * volume;
            oldSong.volume -= volumeStep;
            newSong.volume += volumeStep;
            yield return null;
        }
        oldSong.stop();        
    }    
    
}
