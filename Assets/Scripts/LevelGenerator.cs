﻿using UnityEngine;
using System.Collections;

public class LevelGenerator : MonoBehaviour {
    public enum SpawnType { None, Planet, AsteroidBelt, Random}
    public int galaxySize = 5;
    public bool galaxyCreated = false;
    public int levelSize;
    public int zoneSize;
    public float zoneBuffer;
    public float maxZoneBuffer = 1024;
    public int minLevelSize = 2;
    public int maxLevelSize = 10;
    public KeyCode inputKey = KeyCode.F1;
    
    public float junkFieldProbability = 0.50f;
    public float asteroidProbability = 0.40f;
    public float satelliteProbability = 0.50f;
    public Vector2 sectorSize = new Vector2(8192, 8192);
    private bool[,] sector;
    public int numSectors = 64;
    GameObject solarSystemPrefab;
    GameObject satellitePrefab;
    GameObject junkPrefab;
    public bool setPLayer = false;
    

	// Use this for initialization
	void Start () {

        //solarSystem = generateSolarSystem();
        solarSystemPrefab = (GameObject)Resources.Load("Prefabs/Solar System/SolarSystem",      typeof(GameObject));
        satellitePrefab =   (GameObject)Resources.Load("Prefabs/Solar System/SatelliteSpawner", typeof(GameObject));
        junkPrefab =        (GameObject)Resources.Load("Prefabs/Solar System/JunkSpawner",      typeof(GameObject));
                
        int i, x, y, r;
        i = 0;
        r = Mathf.RoundToInt(Mathf.Sqrt(numSectors));
        sector = new bool[r, r];
        while (i < numSectors)
        {
            
            x = Mathf.FloorToInt( (float)i / (float) r );
            y = i % r;
            //Debug.Log("i,x,y,r = "+i+", "+x+", "+y+", "+r+", ");
            sector[x,y] = false;
            i++;
        }
        if (setPLayer)
        {
            Transform player = GameHelper.getPlayer().transform;
            Vector3 playerposition = sectorSize * Mathf.Floor(r / 2);
            player.position = new Vector3(playerposition.x, playerposition.y, player.position.z);
        }
	}
    public Vector2 getRandomSectorCenter()
    {
        Vector2 s;
        int r = Mathf.RoundToInt(Mathf.Sqrt(numSectors));
        int x,y;
        int j = 100;
        do
        {
            x = Random.Range(0, r);
            y = Random.Range(0, r);
            if (sector[x, y] == false)
            {
                sector[x, y] = true;
                break;
            }
            j--;
        }
        while (j>0);
        s = new Vector2((float)x, (float)y);
        s = Vector2.Scale(s, sectorSize)+(sectorSize / 2);
        return s;
    }    
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(inputKey))
        {
            StartCoroutine(generate());
            //generate();
        }
        if (!galaxyCreated)
        {
            galaxyCreated = true;

            int i = 0;
            while (i < galaxySize)
            {
                i++;
                StartCoroutine(generate());
            }

        }
	}
    
    IEnumerator generate()
    //void generate()
    {
        GameObject solarSystem;
        ObjectSpawner starSpawn;
        ObjectSpawner asteroidSpawn;
        ObjectSpawner planetoidSpawn;
        Vector3 position = getRandomSectorCenter();

        solarSystem = (GameObject)GameObject.Instantiate(solarSystemPrefab, position, transform.rotation);        
        starSpawn = solarSystem.transform.Find("StarSpawner").gameObject.GetComponent<ObjectSpawner>();
        asteroidSpawn = solarSystem.transform.Find("AsteroidSpawner").gameObject.GetComponent<ObjectSpawner>();
        planetoidSpawn = solarSystem.transform.Find("PlanetoidSpawner").gameObject.GetComponent<ObjectSpawner>();

        //yield return null;
        Debug.Log("Generating level");
        //randomizeSpawnPosition();
        //yield return new WaitForSeconds(0.5f);
        starSpawn.activateSpawner(true);
        Debug.Log("Yielding1");
        yield return new WaitForSeconds(0.5f);
        Debug.Log("Continue1");
        
        levelSize = Random.Range(minLevelSize, maxLevelSize);
        GameObject star = starSpawn.spawnedObjects[starSpawn.objectCount-1];
        
        float radius = star.GetComponent<CircleCollider2D>().radius;
        zoneBuffer = radius/levelSize * 0.55f;
        if (zoneBuffer > maxZoneBuffer)
            zoneBuffer = maxZoneBuffer;
        zoneSize = Mathf.RoundToInt(radius * 0.1f);

        float orbitRadius = radius + levelSize * (zoneBuffer + zoneSize);
        star.GetComponentInChildren<Orbit>().radius = orbitRadius;
        Debug.Log("Orbit Radius: " + orbitRadius);

        Debug.Log("Creating solar system, zoneBuffer+zoneSize = " + zoneBuffer + zoneSize);
        //yield return null;
        int j = 0;
        SpawnType spawn;
        while (j < levelSize) 
        {
            j++;
            Debug.Log(j+"/"+levelSize);
            spawn = (SpawnType) Random.Range(0, (int)SpawnType.Random );
            //Asteroid Belt fails probability check
            //Debug.Log(j+" THIS TEXT SHOULD SHOW 3 TIMES PER WHILE LOOP");
            if (spawn == SpawnType.AsteroidBelt && Random.Range(0f, 1f) > asteroidProbability)
            {
                spawn = SpawnType.Planet;
            }

            float minRadius = radius * 0.1f + zoneBuffer*(j+1)+zoneSize*j;

            //Debug.Log(j + " THIS TEXT SHOULD SHOW 3 TIMES PER WHILE LOOP");
            Debug.Log(j+" Spawning " + spawn);
            //float speed = Random.Range(0.1f, 0.3f)*Mathf.Sign(Random.Range(-1f,1f));

            switch (spawn)
            {
                case SpawnType.Planet:
                    setSpawnZone(planetoidSpawn,minRadius);
                    planetoidSpawn.activateSpawner(true);
                    //Debug.Log(j+" Yielding2");
                    //yield return new WaitForSeconds(0.5f);
                    //Debug.Log(j+" Continue2");
                    //yield return null;
                    spawnPlanetChildren(planetoidSpawn);
                    break;
                case SpawnType.AsteroidBelt:
                    setSpawnZone(asteroidSpawn, minRadius);
                    asteroidSpawn.activateSpawner(true);
                    //Debug.Log(j+" Yielding3");
                    //yield return new WaitForSeconds(0.5f);
                    //Debug.Log(j+" Continue3");
                    break;
                default:
                    break;
            }
            //Debug.Log(j + " THIS TEXT SHOULD SHOW 3 TIMES PER WHILE LOOP");            
            Debug.Log("Yield");
            yield return new WaitForSeconds(1.0f);
            Debug.Log("Continue");
            
        }

        Debug.Log("Done");
    }

    private void spawnPlanetChildren(ObjectSpawner spawner)
    {
        Debug.Log("Spawning Planet Children");
        float rand = Random.Range(0f, 1f);
        GameObject lastSpawn = spawner.lastSpawn;
        //We make sure there's something to actually spawn stuff around to...
        if (lastSpawn == null) return;

        if (rand <= junkFieldProbability)
        {
            spawnJunkField(lastSpawn);
        }
        if (rand <= satelliteProbability)
        {
            spawnSatellite(lastSpawn);
        }
        //if (rand)
        //spawner.orbitSpeed = speed;
    }

    private void spawnSatellite(GameObject lastSpawn)
    {
        Debug.Log("Spawning Satellites");
        //GameObject satellitePrefab = (GameObject)Resources.Load("Prefabs/Solar System/SatelliteSpawner", typeof(GameObject));
        createSpawner(lastSpawn, satellitePrefab);
        
    }
    private void spawnJunkField(GameObject lastSpawn)
    {
        Debug.Log("Spawning Junkfield");
        //GameObject junkPrefab = (GameObject)Resources.Load("Prefabs/Solar System/JunkSpawner", typeof(GameObject));
        createSpawner(lastSpawn, junkPrefab);
    }

    private void setSpawnZone(ObjectSpawner spawner, float radius)
    {
        spawner.minSpawnRadius = radius;
        spawner.maxSpawnRadius = radius + zoneSize;   
    }
    private void createSpawner(GameObject parentSpawn, GameObject prefab, bool randomSpeed = false, float minSpeed = 1f, float maxSpeed = 5f)
    {
        Debug.Log("prefab " + prefab + " parentSpawn " + parentSpawn);
        GameObject spawner = (GameObject)GameObject.Instantiate(prefab, parentSpawn.transform.position, parentSpawn.transform.rotation);
        spawner.transform.parent = parentSpawn.transform;
        ObjectSpawner objectSpawner = spawner.GetComponent<ObjectSpawner>();
        objectSpawner.orbitSpeed = (randomSpeed ? Random.Range(minSpeed, maxSpeed) : objectSpawner.orbitSpeed);
        objectSpawner.activateSpawner(true);
        //objectSpawner.activate = true;
        Debug.Log("Created spawner");
    }
}
