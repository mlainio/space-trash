﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(DamageDealer))]
public class DamageArea : MonoBehaviour {
    public float damageRadius;
    private DamageDealer damagedealer;
    public float cooldown = 1f;
    private float timer = 0;

	// Use this for initialization
	void Start () {
        damagedealer = gameObject.GetComponent<DamageDealer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (timer > 0)
            timer -= Time.deltaTime;
	}

    void OnTriggerStay2D (Collider2D other)
    {
        if (timer <= 0)
        {
            damagedealer.damageOther(other.gameObject, true, damageRadius);
            timer = cooldown;
        }
            
    }
}
