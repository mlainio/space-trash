﻿using UnityEngine;
using System.Collections;

public class GravityGun : BeamWeapon, IShootable {
    public enum Mode { None = 1, PowerPush = 2, Push = 4, PowerPull = 8, Pull = 16, Hold = 32 };
    public Mode mode = Mode.None;
    public int normalAmmoRate = 5;
    public int powerAmmoRate = 15;
    public int powerModeMultiplier = 5;
	public AudioSource altLoopSound;
    private float activationDelay = 0.33f;
    private float beamTime = 0.5f;
    private float beamTimer = 0;
    private float delayTimer = 0f;
    bool timer = false;

    
    public override void Update()
    {
        if (delayTimer > 0f)
        {
            delayTimer -= Time.deltaTime;
        }
        else if (timer)
        {            
            fire = true;            
            beamTimer = beamTime;                      
        }        

        if (beamTimer > 0f)
            beamTimer -= Time.deltaTime;
        else if (!timer && fire)
        {            
            fire = false;            
            mode = Mode.None;
        }

        base.Update();
            
    }
    private bool isPowerMode()
    {   
        if (((Mode.PowerPush | Mode.PowerPull) & mode) == mode)
            return true;
        return false;
    }
    protected override void shoot()
    {
        if (timer)
        {
            //fire = false;
            //beamTimer = beamTime;
            timer = false;
        }

        if (isPowerMode())
            ammoRate = powerAmmoRate;
        else
            ammoRate = normalAmmoRate;

        damageDealer.damage.effectVector = getForceEffect();
        
        base.shoot();
        //fire = false;
    }
    private Vector2 getForceEffect()
    {
        Vector2 force = transform.up;
        switch (mode)
        {
            case Mode.PowerPush:
                force *= damageDealer.damage.value * powerModeMultiplier;
                break;
            case Mode.Push:
                force *= damageDealer.damage.value;
                break;
            case Mode.PowerPull:
                force *= -damageDealer.damage.value * powerModeMultiplier;
                break;
            case Mode.Pull:
                force *= -damageDealer.damage.value;
                break;            
            
            default:
                break;
        }
        return force;
    }
    private void shootBegin(Mode mode)
    {
        delayTimer = activationDelay;
        timer = true;        
        //fire = true;
        this.mode = mode;
    }
    private void shootStop() {
        if (timer)
        {
            //fire = true;            
            delayTimer = 0;
        }
        else
            beamTimer = beamTime;
            //fire = false;
    }
    private void shootHold(Mode mode)
    {
        if (!timer)
        {
            this.mode = mode;
            beamTimer = beamTime;
            fire = true;
        }
    }
    void IShootable.fireMain()
    {
        shootBegin(Mode.PowerPush);
		if (loopSound != null && !loopSound.isPlaying) loopSound.Play();
    }
    //if click was short enough, we apply large constant force
    void IShootable.stopMain()
    {
        shootStop();  
		if (loopSound != null && loopSound.isPlaying) loopSound.Stop();
    }

    //apply constant small force
    void IShootable.holdMain()
    {
        shootHold(Mode.Push);
    }

    void IShootable.fireAlt()
    {
        shootBegin(Mode.PowerPull);
		if (altLoopSound != null && !altLoopSound.isPlaying) altLoopSound.Play();
    }

    void IShootable.stopAlt()
    {
        shootStop();
		if (altLoopSound != null && altLoopSound.isPlaying) altLoopSound.Stop();
    }

    void IShootable.holdAlt()
    {
        shootHold(Mode.Pull);
    }
}
