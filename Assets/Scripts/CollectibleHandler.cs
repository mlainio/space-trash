﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectibleHandler : MonoBehaviour {
	public int pickUpCount;
	public string countText;
    private float oldmass;
    private List<ResourceContainer> listContainers = new List<ResourceContainer>(); //list of all containers within the object.
    private bool updateResources;
    private GameObject player;

    void Awake()
    {
        player = GameHelper.getPlayer();
        oldmass = player.rigidbody2D.mass;
        ResourceMeta.initResources();           //creates templates in ResourceMeta
    }
    void Start(){

        

        //Debug.Log(oldmass);
		pickUpCount = 0;		
        fetchContainers();        
        SetCountText();
        updateParentMass();
	}
    void Update()
    {
        if (updateResources)
        {
            updateParentMass();
            SetCountText();
            updateResources = false;
        }

    }
	//void OnCollisionEnter2D(Collision2D other) //changed from trigger to collision
    void OnTriggerEnter2D(Collider2D other)
    {  
		if(other.gameObject.tag == "PickUp"){
            ResourcePhysical otherResource = other.gameObject.GetComponent<ResourcePhysical>();            
            if (otherResource == null)
                Debug.Log("Null Object!");
            //find suitable container
            bool added = false;
            foreach (ResourceContainer rc in listContainers)
            {   
                if (rc.isLegalResource(otherResource.ResourceType) && rc.hasRoomFor(otherResource))
                {
                    added = rc.addResource(otherResource, otherResource.Quantity);
                }
                
                if (added)
                    break;
            }
            if (added)
            {
                other.gameObject.SendMessage("setToDestroy", 0f);
                other.gameObject.SendMessage("setCollected", true);
                SetCountText();
                updateParentMass();                
            }
		}
	}

    private void updateParentMass()
    {
        float m = oldmass;
        foreach (ResourceContainer rc in listContainers)
        {
            m += rc.mass;
        }
        //Debug.Log("Massgain: Old " + oldmass + " , Total: " + m + "(" + rigidbody2D.mass + ")");
        player.rigidbody2D.mass = m;
    }

	void SetCountText(){
		
        countText = "Resources:\n";
        foreach (ResourceContainer rc in listContainers)
        {
            countText += rc;
        }

	}
    void fetchContainers()
    {
        Debug.Log("fetching all containers...");
        ResourceContainer[] array = gameObject.transform.parent.GetComponentsInChildren<ResourceContainer>();
        foreach (ResourceContainer cont in array)
        {
            listContainers.Add(cont);
        }
    }

    internal IEnumerable<ResourceContainer> getContainers()
    {
        return listContainers;
    }

    public void resourceUpdateRequest()
    {
        updateResources = true;
    }
}
