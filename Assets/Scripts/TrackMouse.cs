﻿using UnityEngine;
using System.Collections;

public class TrackMouse : MonoBehaviour {

    // speed is the rate at which the object will rotate
    ThrusterController.turnDirection turnDir;
    public float turnTorque = 100;
    public float maxTorque;
    public float deadAngle = 5;
	public float angularVelocity;
	public float angle;
    public float reverseTurnMultiplier = 2f;
    //public float reverseTurnVariable = 1f;
    public float optimAngle = 5;
    public float calculatedOptimal;
    //public float reverseMult = 0.200f;
    //public float reverseMult2 = 0.200f;
    public float torque;
    GameObject guiObject;
    MouseCursor mouseCursor;
    
	public float angledir;

	// Use this for initialization
	void Start () {
        guiObject = GameObject.Find("GUI_Object");
        mouseCursor = guiObject.GetComponent<MouseCursor>();
        maxTorque = turnTorque;

	}

	// Update is called once per frame
	void FixedUpdate () {
		angularVelocity = rigidbody2D.angularVelocity;
        Vector2 targetPoint = (Vector2) mouseCursor.getWorldPosition();
        Vector2 targetDirection = targetPoint - (Vector2) transform.position;
        Vector2 forward = (Vector2) transform.up;
		angledir = AngleDir (forward, targetDirection);
		angle = Vector2.Angle (forward, targetDirection);
        if (angledir > 0 && angle > deadAngle)
        {
            //Debug.Log ("turning left "+angle+", "+angledir);
            turnDir = ThrusterController.turnDirection.Left;
            turnTo(angledir, angle);

            //turn left
        }
        else if (angledir < 0 && angle > deadAngle)
        {
            // turn right
            //Debug.Log("turning right " + angle + ", " + angledir);
            turnDir = ThrusterController.turnDirection.Right;
            turnTo(angledir, angle);

        }
        else turnDir = ThrusterController.turnDirection.Center;
        //Debug.Log(turnDir);
	}
	/*
	 * This returns a negative number if B is left of A, positive if right of A, or 0 if they are perfectly aligned.
	 */	  
	public static float AngleDir(Vector2 A, Vector2 B)
	{
		return (-A.x * B.y + A.y * B.x)*-1;
	}
	public void turnTo(float direction, float angle) {
		direction = Mathf.Sign(direction);
		//torque = turnTorque;
        calculatedOptimal = getOptimalAngle();
        //torque = ((angle > optimAngle) ? 1f : angle / optimAngle) * turnTorque;
        torque = ((angle > calculatedOptimal) ? 1f : angle / calculatedOptimal) * turnTorque;

        if (applyReverseTurn (angle) ) {
        
            //reverse thrust direction
            if (!isOppositeDir(rigidbody2D.angularVelocity, direction))
            {   
                direction *= -1;                
            }

            turnDir = (ThrusterController.turnDirection)((int)turnDir + 1);
        
            torque = ((angle > calculatedOptimal) ? 0.01f : 1 - angle / (calculatedOptimal)) * turnTorque;
		}
        
        // clamp torque
        if (torque > maxTorque) torque = maxTorque;
       
		rigidbody2D.AddTorque(direction*torque);
	}
    float getOptimalAngle()
    {   float m = rigidbody2D.mass;
        float optimal = (Mathf.Pow(angularVelocity, 2f) * m) / 1000 * reverseTurnMultiplier;
        return optimal+0.5f;
    }
	bool applyReverseTurn(float angle) {
        //float m = rigidbody2D.mass;        
        //added (m*turntorqu) /
        //if (angle < (Mathf.Abs(angularVelocity)*m/turnTorque)*0.30f)
        //if ( angle < (Mathf.Pow(angularVelocity,2f) * m) / 1000 * reverseTurnMultiplier)
        calculatedOptimal = getOptimalAngle();
        if (angle < calculatedOptimal)
			return true;
		else		
			return false;
	}
    bool isOppositeDir(float from, float to)
    {
        //Debug.Log(Mathf.Sign(from) * Mathf.Sign(to));
        if (Mathf.Sign(from) * Mathf.Sign(to) == 1)               
            return false;
                
        return true;
    }
    public ThrusterController.turnDirection getTurnDirection()
    {
        return turnDir;
    }
}
