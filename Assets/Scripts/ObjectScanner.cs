﻿using UnityEngine;
using System.Collections.Generic;

public class ObjectScanner : MonoBehaviour {
    public float radius = 15f;
    public LayerMask layerMask;
    public float scanInterval = 1.0f;
    Timer scanTimer;
    List<GameObject> scannedObjects;

	// Use this for initialization
	void Start () {

        scanTimer = new Timer(scanInterval, true);
        scannedObjects = new List<GameObject>();
	}
	
	// Update is called once per frame
	void Update () {

        if (scanTimer.isFinished())
        {
            scanTimer.reset();

            List<GameObject> newObjects = new List<GameObject>();
            Collider2D[] colliders = Physics2D.OverlapCircleAll((Vector2)transform.position, radius, layerMask);

            foreach (Collider2D col in colliders)
            {
                GameObject go;
                if (col.gameObject.tag == "Sprite")
                {
                    go = col.transform.parent.gameObject;
                }
                else
                    go = col.gameObject;

                newObjects.Add(go);
            }
            scannedObjects = newObjects;

            //Debug.Log("Scanner found " + scannedObjects.Count + " objects");

            
        }
        else
            scanTimer.Update(Time.deltaTime);
	}

    public GameObject getNearestObject()
    {
        GameObject nearestObject = null;
        float nearestDistance = Mathf.Infinity;
        foreach (GameObject go in scannedObjects)
        {
            float dist = Vector2.Distance(transform.position, go.transform.position);
            if (dist < nearestDistance)
                nearestDistance = dist;
            nearestObject = go;
        }
        return nearestObject;
    }
    public GameObject getNearestObjectWithTag(string tag)
    {
        GameObject nearestObject = null;
        float nearestDistance = Mathf.Infinity;
        foreach (GameObject go in scannedObjects)
        {
            if (go != null && go.tag == tag)
            {
                float dist = Vector2.Distance(transform.position, go.transform.position);
                if (dist < nearestDistance)
                    nearestDistance = dist;
                nearestObject = go;
            }
            
        }
        return nearestObject;
    }
    public GameObject getProbableCollision(Vector2 direction, float distance)
    {
        GameObject go = null;
        float offset = GameHelper.getColliderRadius(transform.collider2D);

        RaycastHit2D hit = Physics2D.Raycast(
                transform.position + transform.up * offset,
                transform.up,
                radius,
                layerMask);

        if (hit.collider != null)
        {
            go = hit.transform.gameObject;
            if (go != null)
                if (go.tag == "Sprite")
                    go = hit.transform.parent.gameObject;
        }

        return go;
    }
    
}
