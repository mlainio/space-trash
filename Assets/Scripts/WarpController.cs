﻿using UnityEngine;
using System.Collections;

public class WarpController : MonoBehaviour {
    public KeyCode warpKey = KeyCode.G;
    public float minWarpDistance = 1024;
    MouseCursor cursor;
    private Vector3 warpTarget;
	// Use this for initialization
	void Start () {
        cursor = GameHelper.getCursor();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(warpKey))
        {
            warpTarget = cursor.getWorldPosition();
            warpTo(warpTarget);
        }
	}

    private void warpTo(Vector3 warpTarget)
    {
        warpTarget.z = transform.position.z;
        Vector3 direction = warpTarget - transform.position;
        transform.position = warpTarget;
    }
}
