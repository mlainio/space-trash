﻿using UnityEngine;
using System.Collections;

public class ParticleController : MonoBehaviour {
	
    public void stopParticleLooping()
    {
        gameObject.GetComponent<ParticleSystem>().loop = false;
    }
}
