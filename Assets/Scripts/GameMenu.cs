using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameMenu : MonoBehaviour {
	bool draw = false;
	bool customizeMenu = false;
	private string[] toolbarStrings = {"Laser","Blaster","Railgun","Gravity Gun"};
	private int toolbar1 = 100; 
	private int toolbar2 = 100;
	private int toolbar3 = 100;
	private int toolbar4 = 100;
	private int toolbar5 = 100;
	private int selectionGridInt1 = 100;
	private int selectionGridInt2 = 100;
	private int selectionGridInt3 = 100;
	private int selectionGridInt4 = 100;
	private int selectionGridInt5 = 100;
	private string[] selectionStrings = {"0", "1", "2"}; //<- add "3" here to get additional grid slot
	public GUIStyle style;
	GameObject player;
	Camera mainCam;
	WeaponController weaponControl;
	WeaponGroup weaponGroup;
	Trading trade;
	CollectibleHandler colHand;
	private List<Transform> listSlots = new List<Transform>();
	public GameObject wpn0;
	public GameObject wpn1;
	public GameObject wpn2;
	public GameObject wpn3;

	public float laserCost = 200f;
	public float blasterCost = 200f;
	public float railCost = 200f;
	public float gravityCost = 200f;

	//public ResourceMeta.Type buyResource1;
		
	// Use this for initialization
	void Start () {
		player = GameHelper.getPlayer();
		weaponControl = player.GetComponentInChildren<WeaponController>();
		mainCam = GameHelper.getCamera();
		trade = player.GetComponentInChildren<Trading>();
		colHand = player.GetComponentInChildren<CollectibleHandler>();
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			if(draw==false /*&& customizeMenu==false*/){
				draw=true;
				Time.timeScale = 0;
				mainCam.GetComponentInChildren<PlayerHUD>().setGUI(false);
			}
			/*else if(draw==false && customizeMenu==true){
				customizeMenu=false;
				draw=true;
			}*/

			else if(draw==true){
				draw=false;
				mainCam.GetComponentInChildren<PlayerHUD>().setGUI(true);
				Time.timeScale = 1;
			}
			
		}
	}
	
	void OnGUI () {
		GUI.color = Color.cyan;

		//GUI.Label(new Rect(Screen.width/2,Screen.height/2,100,50),"Press E to trade");
		
		if (draw) {
			/*if (GUI.Button (new Rect (Screen.width/2-100, 150, 200, 100), "Customize")){
				draw = false;
				customizeMenu = true;
			}*/

			if (GUI.Button (new Rect (Screen.width/2-100, 300, 200, 100), "Main Menu")) {
				Application.LoadLevel ("Menu");
				Time.timeScale=1;
			}

			if (GUI.Button (new Rect (Screen.width/2-100, 450, 200, 100), "Quit")) {
				Application.Quit ();	
			}

		}
		if(customizeMenu) {

			trade.setDrawNotification(false);
			listSlots = weaponControl.getSlotslist();

			selectionGridInt1 = GUI.SelectionGrid (new Rect (Screen.width/2-150, 50, 60, 40), selectionGridInt1, selectionStrings, 2);
			//selectionGridInt1 = listSlots[0].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();
			if(selectionGridInt1 == 0){
				changeWeaponGroup(0,0);
				selectionGridInt1=100;
			}
			if(selectionGridInt1 == 1){
				changeWeaponGroup(0,1);
				selectionGridInt1=100;
			}
			if(selectionGridInt1 == 2){
				changeWeaponGroup(0,2);
				selectionGridInt1=100;
			}
			if(selectionGridInt1 == 3){
				changeWeaponGroup(0,3);
				selectionGridInt1=100;
			}
				
	
			toolbar1 = GUI.Toolbar (new Rect (Screen.width/2-50, 50, 350, 40), toolbar1, toolbarStrings);
			if(toolbar1 == 0 && trade.getCredits()>= laserCost){
				changeWeapon(0,wpn0);
				trade.deductCredits(laserCost);
				toolbar1=100;

			}
			if(toolbar1 == 1 && trade.getCredits()>= blasterCost){
				changeWeapon(0,wpn1);
				trade.deductCredits(blasterCost);
				toolbar1=100;
							}
			if(toolbar1 == 2 && trade.getCredits()>= railCost){
				changeWeapon(0,wpn2);
				trade.deductCredits(railCost);
				toolbar1=100;
			}
			if(toolbar1 == 3 && trade.getCredits()>= gravityCost){
				changeWeapon(0,wpn3);
				trade.deductCredits(gravityCost);
				toolbar1=100;
			}

			selectionGridInt2 = GUI.SelectionGrid (new Rect (Screen.width/2-150, 100, 60, 40), selectionGridInt2, selectionStrings, 2);
			//selectionGridInt2 = listSlots[1].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();
			if(selectionGridInt2 == 0){
				changeWeaponGroup(1,0);
				selectionGridInt2=100;
			}
			if(selectionGridInt2 == 1){
				changeWeaponGroup(1,1);
				selectionGridInt2=100;
			}
			if(selectionGridInt2 == 2){
				changeWeaponGroup(1,2);
				selectionGridInt2=100;
			}
			if(selectionGridInt2 == 3){
				changeWeaponGroup(1,3);
				selectionGridInt2=100;
			}


			toolbar2 = GUI.Toolbar (new Rect (Screen.width/2-50, 100, 350, 40), toolbar2, toolbarStrings);
			if(toolbar2 == 0 && trade.getCredits()>= laserCost){
				changeWeapon(1,wpn0);
				trade.deductCredits(laserCost);
				toolbar2=100;
			}
			if(toolbar2 == 1 && trade.getCredits()>= blasterCost){
				changeWeapon(1,wpn1);
				trade.deductCredits(blasterCost);
				toolbar2=100;
			}
			if(toolbar2 == 2 && trade.getCredits()>= railCost){
				changeWeapon(1,wpn2);
				trade.deductCredits(railCost);
				toolbar2=100;
			}
			if(toolbar2 == 3 && trade.getCredits()>= gravityCost){
				changeWeapon(1,wpn3);
				trade.deductCredits(gravityCost);
				toolbar2=100;
			}

			selectionGridInt3 = GUI.SelectionGrid (new Rect (Screen.width/2-150, 150, 60, 40), selectionGridInt3, selectionStrings, 2);
			//selectionGridInt3 = listSlots[2].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();
			if(selectionGridInt3 == 0){
				changeWeaponGroup(2,0);
				selectionGridInt3=100;
			}
			if(selectionGridInt3 == 1){
				changeWeaponGroup(2,1);
				selectionGridInt3=100;
			}
			if(selectionGridInt3 == 2){
				changeWeaponGroup(2,2);
				selectionGridInt3=100;
			}
			if(selectionGridInt3 == 3){
				changeWeaponGroup(2,3);
				selectionGridInt3=100;
			}

			toolbar3 = GUI.Toolbar (new Rect (Screen.width/2-50, 150, 350, 40), toolbar3, toolbarStrings);
			if(toolbar3 == 0 && trade.getCredits()>= laserCost){
				changeWeapon(2,wpn0);
				trade.deductCredits(laserCost);
				toolbar3=100;
			}
			if(toolbar3 == 1 && trade.getCredits()>= blasterCost){
				changeWeapon(2,wpn1);
				trade.deductCredits(blasterCost);
				toolbar3=100;
			}
			if(toolbar3 == 2 && trade.getCredits()>= railCost){
				changeWeapon(2,wpn2);
				trade.deductCredits(railCost);
				toolbar3=100;
			}
			if(toolbar3 == 3 && trade.getCredits()>= gravityCost){
				changeWeapon(2,wpn3);
				trade.deductCredits(gravityCost);
				toolbar3=100;
			}

			selectionGridInt4 = GUI.SelectionGrid (new Rect (Screen.width/2-150, 200, 60, 40), selectionGridInt4, selectionStrings, 2);
			//selectionGridInt4 = listSlots[3].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();

			if(selectionGridInt4 == 0){
				changeWeaponGroup(3,0);
				selectionGridInt4=100;
			}
			if(selectionGridInt4 == 1){
				changeWeaponGroup(3,1);
				selectionGridInt4=100;
			}
			if(selectionGridInt4 == 2){
				changeWeaponGroup(3,2);
				selectionGridInt4=100;
			}
			if(selectionGridInt4 == 3){
				changeWeaponGroup(3,3);
				selectionGridInt4=100;
			}

			toolbar4 = GUI.Toolbar (new Rect (Screen.width/2-50, 200, 350, 40), toolbar4, toolbarStrings);
			if(toolbar4 == 0 && trade.getCredits()>= laserCost){
				changeWeapon(3,wpn0);
				trade.deductCredits(laserCost);
				toolbar4=100;
			}
			if(toolbar4 == 1 && trade.getCredits()>= blasterCost){
				changeWeapon(3,wpn1);
				trade.deductCredits(blasterCost);
				toolbar4=100;
			}
			if(toolbar4 == 2 && trade.getCredits()>= railCost){
				changeWeapon(3,wpn2);
				trade.deductCredits(railCost);
				toolbar4=100;
			}
			if(toolbar4 == 3 && trade.getCredits()>= gravityCost){
				changeWeapon(3,wpn3);
				trade.deductCredits(gravityCost);
				toolbar4=100;
			}

			selectionGridInt5 = GUI.SelectionGrid (new Rect (Screen.width/2-150, 250, 60, 40), selectionGridInt5, selectionStrings, 2);
			//selectionGridInt5 = listSlots[4].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();
			if(selectionGridInt5 == 0){
				changeWeaponGroup(4,0);
				selectionGridInt5=100;
			}
			if(selectionGridInt5 == 1){
				changeWeaponGroup(4,1);
				selectionGridInt5=100;
			}
			if(selectionGridInt5 == 2){
				changeWeaponGroup(4,2);
				selectionGridInt5=100;
			}
			if(selectionGridInt5 == 3){
				changeWeaponGroup(4,3);
				selectionGridInt5=100;
			}

			toolbar5 = GUI.Toolbar (new Rect (Screen.width/2-50, 250, 350, 40), toolbar5, toolbarStrings);
			if(toolbar5 == 0 && trade.getCredits()>= laserCost){
				changeWeapon(4,wpn0);
				trade.deductCredits(laserCost);
				toolbar5=100;
			}
			if(toolbar5 == 1 && trade.getCredits()>= blasterCost){
				changeWeapon(4,wpn1);
				trade.deductCredits(blasterCost);
				toolbar5=100;
			}
			if(toolbar5 == 2 && trade.getCredits()>= railCost){
				changeWeapon(4,wpn2);
				trade.deductCredits(railCost);
				toolbar5=100;
			}
			if(toolbar5 == 3 && trade.getCredits()>= gravityCost){
				changeWeapon(4,wpn3);
				trade.deductCredits(gravityCost);
				toolbar5=100;
			}

			GUI.Label(new Rect(Screen.width/2,10,200,40),"Credits: "+trade.getCredits().ToString());
			GUI.Label(new Rect(Screen.width/2-400,50,250,40),listSlots[0].gameObject.GetComponent<WeaponSlot>().name);
			GUI.Label(new Rect(Screen.width/2-250,50,250,40),listSlots[0].gameObject.GetComponent<WeaponSlot>().getAssignedWeapon().name);
			GUI.Label(new Rect(Screen.width/2-75,50,250,40),listSlots[0].gameObject.GetComponent<WeaponSlot>().getAssignedGroup().ToString());

			GUI.Label(new Rect(Screen.width/2-400,100,250,40),listSlots[1].gameObject.GetComponent<WeaponSlot>().name);
			GUI.Label(new Rect(Screen.width/2-250,100,250,40),listSlots[1].gameObject.GetComponent<WeaponSlot>().getAssignedWeapon().name);
			GUI.Label(new Rect(Screen.width/2-75,100,250,40),listSlots[1].gameObject.GetComponent<WeaponSlot>().getAssignedGroup().ToString());

			GUI.Label(new Rect(Screen.width/2-400,150,250,40),listSlots[2].gameObject.GetComponent<WeaponSlot>().name);
			GUI.Label(new Rect(Screen.width/2-250,150,250,40),listSlots[2].gameObject.GetComponent<WeaponSlot>().getAssignedWeapon().name);
			GUI.Label(new Rect(Screen.width/2-75,150,250,40),listSlots[2].gameObject.GetComponent<WeaponSlot>().getAssignedGroup().ToString());

			GUI.Label(new Rect(Screen.width/2-400,200,250,40),listSlots[3].gameObject.GetComponent<WeaponSlot>().name);
			GUI.Label(new Rect(Screen.width/2-250,200,250,40),listSlots[3].gameObject.GetComponent<WeaponSlot>().getAssignedWeapon().name);
			GUI.Label(new Rect(Screen.width/2-75,200,250,40),listSlots[3].gameObject.GetComponent<WeaponSlot>().getAssignedGroup().ToString());

			GUI.Label(new Rect(Screen.width/2-400,250,250,40),listSlots[4].gameObject.GetComponent<WeaponSlot>().name);
			GUI.Label(new Rect(Screen.width/2-250,250,250,40),listSlots[4].gameObject.GetComponent<WeaponSlot>().getAssignedWeapon().name);
			GUI.Label(new Rect(Screen.width/2-75,250,250,40),listSlots[4].gameObject.GetComponent<WeaponSlot>().getAssignedGroup().ToString());

			GUI.Label(new Rect(Screen.width/2-200,300,500,40),"Prices Laser: "+laserCost+" Blaster: " +blasterCost+" Railgun: " +railCost+ " Gravitygun: " + gravityCost);

			//if (GUI.Button (new Rect (Screen.width/2-100, 300, 200, 100), "Ammo")){
			//	colHand.addInTrade((IResource)buyResource1);
			//}
		}
	}




	public void changeWeapon(int slotnum, GameObject weapon){
		Transform temp = listSlots[slotnum];
		int group = listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();
		GameObject oldWeapon = temp.GetChild(0).gameObject;
		weaponControl.removeWeaponFromGroup((IShootable)oldWeapon.GetComponent<Weapon>(),group);
		Destroy(temp.GetChild(0).gameObject);
		listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().setWeapon(weapon,player);
		listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().activateUpdate(group);
	}

	public void changeWeaponGroup(int slotnum, int newgroup){
		bool con = true;
		Transform temp = listSlots[slotnum];
		int group = listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().getAssignedGroup();
		GameObject oldWeapon = temp.GetChild(0).gameObject;
		GameObject newWeapon = listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().getAssignedWeapon();
		Debug.Log("new group: "+newgroup+" old group: "+group);
		if(newgroup == group){
			Debug.Log("new group "+newgroup+" = old group "+group);
			con = false;
		}
		if(con == true){
			weaponControl.removeWeaponFromGroup((IShootable)oldWeapon.GetComponent<Weapon>(),group);
			Destroy(temp.GetChild(0).gameObject);
			listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().setWeapon(newWeapon,player);
			listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().setAssignedGroup(newgroup);
			listSlots[slotnum].gameObject.GetComponent<WeaponSlot>().activateUpdate(newgroup);
		}
	}

	public void setDraw(bool b){
		draw = b;
	}
	public bool getDraw(){
		return draw;
	}
	public void setCustomizeMenu(bool b){
		customizeMenu = b;
	}
	public bool getCustomizeMenu(){
		return customizeMenu;
	}
}

