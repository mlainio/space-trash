﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour {
    public enum audioType { Music, Ambient, BeamWeapons, ProjectileWeapons, Thrusters, Collisions, AllSounds };
	public float defaultVolume = 0.5f;
	public float defaultRange = 500f;
    private float[] audioVolumes;
	private float[] audioRanges;

    //manual initialize
    public void initialize()
    {
        audioVolumes = new float[(int)audioType.AllSounds];
        for (int i = 0; i < (int)audioType.AllSounds; i++)
        {
            audioVolumes[i] = defaultVolume;
        }
		
		audioRanges = new float[(int)audioType.AllSounds];
        for (int i = 0; i < (int)audioType.AllSounds; i++)
		{
			audioRanges[i] = defaultRange;	
		}
    }

    public float getVolume(audioType type)
    {
        return audioVolumes[(int)type];
    }
    public void setVolume(audioType type, float volume)
    {
        audioVolumes[(int)type] = volume;
    }
	
	public float getRange(audioType type)
	{
		return audioRanges[(int)type];
	}
	public void setRange(audioType type, float range)
	{
		audioRanges[(int)type] = range;
	}
	
}
