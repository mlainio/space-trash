﻿using UnityEngine;
using System.Collections;

public class InitConfig : MonoBehaviour {
    public static bool ignoreMaxZoom = false;
	public float thrusterBaseVolume = 1f;
	public float thrusterAudioRange = 650f;
    
	// Use this for initialization
	void Awake () {
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerShip"), LayerMask.NameToLayer("Projectile"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("EnemyShip"), LayerMask.NameToLayer("EnemyProjectile"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("PlayerShip"), LayerMask.NameToLayer("Starbase"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("EnemyShip"), LayerMask.NameToLayer("EnemyStarbase"), true);        
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Projectile"), LayerMask.NameToLayer("Projectile"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Planet"), LayerMask.NameToLayer("SpaceObject"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Planet"), LayerMask.NameToLayer("PlayerShip"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Planet"), LayerMask.NameToLayer("EnemyShip"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Planet"), LayerMask.NameToLayer("Projectile"), true);
        Physics2D.IgnoreLayerCollision(LayerMask.NameToLayer("Planet"), LayerMask.NameToLayer("EnemyProjectile"), true);
        Star.initStars();
        AudioController audio = GameHelper.getAudioController();
        audio.initialize();
        audio.setVolume(AudioController.audioType.BeamWeapons, 0.25f);
		audio.setVolume(AudioController.audioType.Thrusters, thrusterBaseVolume);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
