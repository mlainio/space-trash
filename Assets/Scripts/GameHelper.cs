﻿using UnityEngine;
using System.Collections;

public static class GameHelper {
    //  Important gameobjects
    public enum KeyObject { MainCamera, Player, GUI, Init, Total };    //Use the name of the tag
    static GameObject[] objects = new GameObject[(int)KeyObject.Total];

    public static Camera getCamera()
    {
        return getObject(KeyObject.MainCamera).GetComponent<Camera>();
        
        //return null;
    }
    public static GameObject getPlayer()
    {
        return getObject(KeyObject.Player);
    }
    public static GameObject getObject(KeyObject k)
    {
        if (objects[(int)k] == null)
        {
            objects[(int)k] = fetchWithTag(k.ToString());
        }

        return objects[(int)k];
    }
    public static AudioController getAudioController()
    {
        return getObject(KeyObject.Init).GetComponent<AudioController>();
    }
    public static GameObject fetchWithTag(string tag)
    {
        GameObject fetched;
        try
        {
            fetched = (GameObject)GameObject.FindGameObjectWithTag(tag);
            return fetched;
                
        }
        catch (MissingReferenceException e) { Debug.Log(e); }

        return null;
    }

    public static MouseCursor getCursor()
    {
        return getObject(KeyObject.GUI).GetComponent<MouseCursor>();
    }
    public static float getColliderRadius(Collider2D col) {
        float r = Mathf.PI;
        if (col != null)
            if (col.GetType() == typeof(PolygonCollider2D))
            {
                float max = 0;
                foreach (Vector2 point in ((PolygonCollider2D)col).points)
                {
                    if (point.magnitude > max)
                        max = point.magnitude;
                }
                r = max;
            }
            else if (col.GetType() == typeof(BoxCollider2D))
            {
                r = ((BoxCollider2D)col).size.magnitude;

            }
            else if (col.GetType() == typeof(CircleCollider2D))
            {
                r = ((CircleCollider2D)col).radius;
            }
            
        return r;
    }
}
