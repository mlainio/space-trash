﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {
	bool draw = false;
	bool displayCredits = false;
	bool blaa = false;
	public Texture logo;
	public GUIStyle style;
	string credits = " Mikko Lainio \n Eero Hannula \n Antti Saarinen";



	// Use this for initialization
	void Start () {
		Screen.showCursor = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown("l")){
			if(draw==false){
				draw=true;
			}
			else if(draw==true){
				draw=false;
			}
		
		}
	}

	void OnGUI () {
		GUI.color = Color.cyan;

		if (!draw) {
				if (GUI.Button (new Rect (Screen.width/2-100, 300, 200, 100), "Load Level")) {
						Application.LoadLevel ("test");
				}
			if (GUI.Button (new Rect (Screen.width/2-100, 450, 200, 100), "Credits")) {				
				if(displayCredits==false){
					displayCredits=true;
				}
				else if(displayCredits==true){
					displayCredits=false;
				}
			}
			if (GUI.Button (new Rect (Screen.width/2-100, 600, 200, 100), "Quit")) {				
				Application.Quit ();
			}
			/*if(GUI.Button (new Rect(Screen.width/2+200,100,100,100), "Hide Blaa")){
				if(blaa==false){
					blaa=true;
				}
				else if(blaa==true){
					blaa=false;
				}
			}

			if (!blaa) {
				if (GUI.Button (new Rect (Screen.width/2+500, 100, 150, 100), "Blaa")) {
					
				}
			}*/
			if (displayCredits){
				if(GUI.Button( new Rect(Screen.width/2-200, 350, 400, 300),credits,style)){
					if(displayCredits==false){
						displayCredits=true;
					}
					else if(displayCredits==true){
						displayCredits=false;
					}
				}
			}

		}
		GUI.DrawTexture(new Rect(Screen.width/2-150,0,300,300),logo,ScaleMode.ScaleToFit);
			
	}
}
