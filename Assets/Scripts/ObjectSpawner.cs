﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectSpawner : MonoBehaviour {
    
    public List<GameObject> spawnedObjects = new List<GameObject>();
    public static int totalObjectCount = 0;
    
    public int objectCount
    {
        get { return spawnedObjects.Count; }
    }
    [System.Serializable]
    public class SpawnObject
    {
        public GameObject obj;
        public int rarity;
        public SpawnObject(GameObject obj, int rarity)
        {
            this.obj = obj;
            this.rarity = rarity;
        }
        public override string ToString()
        {
            return base.ToString()+": "+obj+" ("+rarity+")";
        }
    }
    [SerializeField]
    public List<SpawnObject> spawnList = new List<SpawnObject>();
    //public GameObject parent;
    public bool activate = false;
    public float minSpawnRadius = 1f;
    public float maxSpawnRadius = 10f;
    public int rarityRangeMin = 1;
    public int rarityRangeMax = 100;
    public int shuffleRandomCount = 2;
    public int raritySumLimit = 500;
    public int spawnCountLimit = 100;
    public bool autoSpawn;
    public int maxSpawnedObjects = 5;
    public float spawnInterval = 10f;
    public bool limitCountByMass = false;
    public bool setParentAsParent = false;
    
    private float massCount;
    private Timer spawnTimer;
    private Timer cleanUpTimer = new Timer(1f, true);
	
	//where to spawn
	public enum Position{
		SpawnerPosition, MousePosition
	};
	public Position spawnPosition;

    //public float minRarityToSpawn= 0.1f;
    public bool forceOnOrbit = false;
    public string OrbitTag = "Planet";
    public float orbitSpeed = 1f;    
    public float orbitSpeedVariance = 0.15f;            //make sure itse between 0 and 1
    public bool randomOrbitDirection = false;
    
    private int spawnCount = 0;
    private int raritySum = 0;
    public KeyCode spawnKey = KeyCode.None;
    private float timeout = 10;
    private float timeoutTimer;

    public bool randomizeForce;
    public float randomForceMultiplier = 100f;
    private int maxSpawnPerFrame = 50;

	// Use this for initialization
	void Start () {
        timeoutTimer = timeout;
        spawnTimer = new Timer(spawnInterval, false);
        if (randomOrbitDirection)
            orbitSpeed *= Mathf.Sign(Random.Range(-1f, 1f));


        if (gameObject.rigidbody2D == null)
            limitCountByMass = false;
	}

	// Update is called once per frame
	void Update () {

        if (cleanUpTimer.isFinished())
        {
            spawnedObjects.RemoveAll(item => item == null);
            cleanUpTimer.reset();
        }
        else cleanUpTimer.Update(Time.deltaTime);
        
        // debug function
        if (Input.GetKeyDown(spawnKey)) {
            activate = true;
        }

        if (activate)
        {
            activateSpawner();            
        }
        if (autoSpawn)
        {            
            if (spawnTimer.isFinished()) 
            {                
                Debug.Log("Autospawning, limit is " + spawnedObjects.Count + "/" + spawnCountLimit);
                if (spawnedObjects.Count < spawnCountLimit)
                    spawn();
            }

            if (!spawnTimer.isActive())
                spawnTimer.reset();
            else
                spawnTimer.Update(Time.deltaTime);
        }
	
	}
    public void activateSpawner(bool activate = false)
    {
        if (activate)
        {
            this.activate = true;
            timeoutTimer = timeout;
        }
            
        int i = 0;
        //Debug.Log("Spawning");
        while (i < maxSpawnPerFrame && spawnCount < spawnCountLimit)
        {
            i++;
            spawn();

            if (spawnCount >= spawnCountLimit || raritySum >= raritySumLimit)            {

                Debug.Log("SpawnCount or RaritySum limit exceeded");
                if (!limitCountByMass || massCount >= gameObject.rigidbody2D.mass)
                {
                    Debug.Log("End Spawning");
                    this.activate = false;
                    spawnCount = 0;
                    raritySum = 0;
                    massCount = 0;
                    break;
                }
            }
            if (timeoutTimer <= 0)
            {
                Debug.Log("Timeout! End spawning");
                this.activate = false;
                timeoutTimer = timeout;
                spawnCount = 0;
                raritySum = 0;
                massCount = 0;
                break;
            }
        }
        timeoutTimer -= Time.deltaTime;
    }
    //Spawn a single object
    private void spawn()
    {
        if (spawnList.Count < 0)
            return;
        
        float rarity = RandomShuffle(rarityRangeMin, rarityRangeMax, shuffleRandomCount);
        float radius = maxSpawnRadius - minSpawnRadius;
        
        //randomize position
        Vector3 pos = (Vector3)(Random.insideUnitCircle * radius);
		
		pos = pos + pos.normalized * minSpawnRadius + transform.position;
		
		if (spawnPosition == Position.MousePosition) {
			pos = (Vector2)(GameHelper.getCursor().getWorldPosition());
		}
        
        
        //select random spawn
        //Debug.Log(pos);
        SpawnObject spawn = spawnList[Random.Range(0, spawnList.Count)];
        //Debug.Log(spawn);

        try
        {
            //if (spawn.rarity >= rarity * minRarityToSpawn && spawn.rarity <= rarity && raritySum + spawn.rarity <= raritySumLimit)
            if (spawn.rarity <= rarity && raritySum + spawn.rarity <= raritySumLimit && spawn.obj != null)
            {
                GameObject obj = (GameObject)GameObject.Instantiate(spawn.obj, pos, gameObject.transform.rotation);
                if (setParentAsParent)
                    obj.transform.parent = this.transform.parent;
                else
                    obj.transform.parent = this.transform;
                spawnedObjects.Add(obj);
                raritySum += spawn.rarity;
                spawnCount++;
                totalObjectCount++;

                
                Debug.Log("Spawned " + obj + " rarity " + spawn.rarity + " (" + raritySum + "/" + raritySumLimit + ") SpawnCount/Global Count: "+spawnedObjects.Count+"/"+totalObjectCount);
                
                if (limitCountByMass)
                    massCount += obj.rigidbody2D.mass;

                if (forceOnOrbit)
                {
                    SimpleOrbit orbit = obj.GetComponentInChildren<SimpleOrbit>();
                    if (orbit == null)
                    {
                        orbit = obj.AddComponent<SimpleOrbit>();
                    }

                    orbit.targetTag = OrbitTag;
                    if (orbitSpeedVariance > 0)
                        orbitSpeed += Random.Range(-orbitSpeedVariance, orbitSpeedVariance);
                    orbit.orbitSpeed = orbitSpeed;
                    orbit.randomDirection = false;
                    //orbit.randomDirection = randomOrbitDirection;                    
                    orbit.findNearestOrbit();
                    orbit.setOnOrbit();                                                            
                }
                
                RandomMomentum rand = obj.GetComponent<RandomMomentum>();
                if (rand == null)
                    rand = obj.AddComponent<RandomMomentum>();

                rand.applyRandomizedMomentum(randomForceMultiplier);
                //rand.start = true;
            }
        }
        catch (MissingReferenceException e) { Debug.Log(e); }        
    }

    private int RandomShuffle(int min, int max, int shuffle)
    {
        int count = 0;
        float random = 0;
        while (count < shuffle)
        {
            random += Random.Range(min, max);
            count++;
        }
        return Mathf.RoundToInt(random / shuffle);
    }

    public GameObject lastSpawn
    {
        get {
            //Debug.Log("Last Spawn: " + spawnedObjects.Count + "/" + totalObjectCount + "/" + spawnCount);
                if (spawnedObjects.Count > 0)
                    return spawnedObjects[spawnedObjects.Count - 1];            
                return null;
            }
    }

    
}
