﻿using UnityEngine;
using System.Collections;

public class StarProperties : MonoBehaviour {
    public Gradient starGradient;    
    public Star.Type type;
    public Star.Size size;
    public static float baseRadius = 2048;
    public static double baseMass = 6e+08;
    public static float baseTemperature = 10;
    public static float baseLuminosity = 5000;
    public static float baseHalo = 75;
    public static float baseDamageRadius = 64;
    public static float baseDeadRadius = 38;
    public static float baseMinZoom = 20000;
    public static float baseMaxZoom = 50000000;
    [System.Serializable]
    public struct ColorLocation
    {
        public float min;
        public float max;
    }
    
    // Star.Type.Random is the last enum of Star.Type i.e. it is also the number of values before it
    // -> usable in iterating an array!
    [SerializeField]
    public ColorLocation[] loc; 
    
    //public float 
    

	// Use this for initialization
	void Start () {

        loc = new ColorLocation[(int)Star.Type.Random];
        // Generate color range in gradient
        for (int i = 0; i < (int) Star.Type.Random; i++)
        {
            loc[i].min = (float)(1f / (float)Star.Type.Random) * i;
            loc[i].max = (float)(1f / (float)Star.Type.Random) * (i+1);
            //Debug.Log(loc[i].min + "/" + loc[i].max);
        }
        createStar();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void createStar()
    {
        Light light = gameObject.GetComponentInChildren<Light>();
        ParticleSystem particle = gameObject.GetComponentInChildren<ParticleSystem>();
        GameObject sunburn = gameObject.GetComponentInChildren<DamageArea>().gameObject;
        GravityWell gravitywell = gameObject.GetComponent<GravityWell>();
        ZoomIcon zoom = gameObject.GetComponent<ZoomIcon>(); ;
        CircleCollider2D collider = gameObject.GetComponent<CircleCollider2D>();
        Behaviour halo = (Behaviour)GetComponent("Halo");

        Debug.Log("Creating a star");

        Star star = new Star();
        star = Star.getProperties(type);
        
        gravitywell.mass = baseMass * star.mass;
        gravitywell.deadRadius = baseDeadRadius * star.radius;
        collider.radius = baseRadius * star.radius;
        //zoom.zoomThreshold = Mathf.RoundToInt(baseMinZoom * star.radius);
        zoom.zoomThreshold = Mathf.RoundToInt(baseMinZoom);
        zoom.maxThreshold = Mathf.RoundToInt(baseMaxZoom * star.radius);

        particle.startSize *= star.radius;
        float colorLocation = Random.Range(loc[(int)star.type].min, loc[(int)star.type].max);
        //Debug.Log(colorLocation);
        Color color = starGradient.Evaluate( Random.Range( loc[(int)star.type].min,  loc[(int)star.type].max ) );
        //Debug.Log(color);
        particle.startColor = color;
        
        //Particle part = particle.GetParticles();

        light.color = color;
        light.range = baseLuminosity * star.luminosity;        

        sunburn.GetComponent<DamageDealer>().damage.value = baseTemperature * star.temperature;
        Debug.Log(sunburn);
        sunburn.GetComponent<DamageArea>().damageRadius = baseDamageRadius * star.radius;
        sunburn.GetComponent<CircleCollider2D>().radius = baseDamageRadius * star.radius;

        //float orbitRadius = star.radius * 8096;
        //gameObject.GetComponentInChildren<Orbit>().radius = orbitRadius;

        Debug.Log("Created star type " + star.type + ", R: " + star.radius);
    }
}
