﻿using UnityEngine;
using System.Collections;

public class OnOff : MonoBehaviour {
	ParticleSystem thrust;
	
	// Use this for initialization
	void Start () {
		thrust = this.particleSystem;
		thrust.Stop();
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetAxis ("Vertical") > 0) {
			thrust.Play();
	
		}
		if (Input.GetAxis ("Vertical") <= 0) {
			thrust.Stop();
				
		}
	}
}
