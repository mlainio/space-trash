﻿using UnityEngine;
using System.Collections;

public class ResourceFactory : MonoBehaviour {

    public ResourceMeta.Type producedResource;
    public float productionRate = 1.0f;       //production rate per second
    public ResourceMeta.Type consumedResource;
    public float consumptionRate = 1.0f;      //consumptionrate per second    
    private float produced = 0;
    private float consumed = 0;
    
    public GameObject owner;    
    private ResourceDistributor distributor;
    private ResourceMeta toBeConsumed = new ResourceMeta();
    /*
     * Checks for demand
     * Produces resources and consumes resource
     * Sends produced resource to controller
     * Methods to halt / continue production
     * 
     */

	// Use this for initialization
	void Start () {
        try
        {
            distributor = owner.GetComponentInChildren<ResourceDistributor>();            
        }
        catch (MissingComponentException e)
        {
            Debug.Log(e);
        }
        
	}
	
	// Update is called once per frame
	void Update () {
        int need = distributor.checkDemand(producedResource);
        if ( need > 0 )
            produceAndConsume(need);

	}
    void produceAndConsume(int need)
    {
        //fetch resources to consume
        try
        {
            if (consumed <= 0 && produced < need)
            {
                //let's get a new chunk to consume and create the produced resource
                
                ResourceMeta temp = distributor.require(consumedResource, Mathf.CeilToInt(consumptionRate));
                consumed = temp.Quantity;
                                                         
            }
        }
        catch (System.NullReferenceException e)
        {
            //Debug.Log("no resource "+consumed);
            //HudText to inform no more crystals in cargo.
        }       
        
        //consume and produce
        float value = consumptionRate * Time.deltaTime;

        if (consumed - value < 0 && consumed >= 0)
        {
            value = consumed;
        }

        if (value > 0)
        {
            consumed -= value;
            produced += value * (productionRate / consumptionRate);
        }
        
        //supply
        int units = Mathf.FloorToInt(produced);
        //Debug.Log("Produced units: " + units + " Need " + need);

        if (units > 0 && need > 0)
        {
            ResourceMeta newProduction = new ResourceMeta();
            newProduction = (ResourceMeta)newProduction.createFromTemplate(ResourceMeta.getTemplate(producedResource), 0f);
            newProduction.Quantity = (units > need) ? need : units;
            produced -= newProduction.Quantity;

            distributor.supply(newProduction);
        }
        //Debug.Log("Produced " + produced + ", Consumed " + consumed + ", Need " + need);
                
    }
}
