﻿import System.Collections.Generic;
import System.Xml.Serialization;

@XmlRoot("SaveState")
public class StateContainer {
	@XmlArray("Player")
	@XmlArrayItem("Position")
	public var Player : List.<Position> = new List.<Position>();
}