﻿import System.Collections.Generic;
import System.Xml;
import System.IO;

function Start() {
    // TESTING
    //var xmlData : String = '<SaveState><Player><Position><X>10</X><Y>5</Y></Position></Player></SaveState>';
    //var container : StateContainer = StateContainer.LoadFromText(xmlData);
    ReadXML();
    // TESTING
}
 
function ReadXML() {
    var localInputFile = "Assets/savestate.xml";
    var serializer : XmlSerializer = new XmlSerializer(StateContainer);
    var stream : Stream = new FileStream(localInputFile, FileMode.Open);
    var container : StateContainer = serializer.Deserialize(stream) as StateContainer;
    stream.Close();
   	Debug.Log ("xmlData: " + container);
}

function WriteXML() {
	var localOutputFile = "Assets/savestate.xml";
    var serializer : XmlSerializer = new XmlSerializer(StateContainer);
    var stream : Stream = new FileStream(localOutputFile, FileMode.Create);
    serializer.Serialize(stream, this);
    stream.Close();
}