﻿using UnityEngine;
using System.Collections;

public class PlayerHUD : MonoBehaviour {
    public GUIStyle style;
    CollectibleHandler collectibles;
    LineRenderer lineRender;
    GameObject player;
    MouseCursor cursor;
    public float healthBarWidth = 48f;
    public float healthBarHeight = 12f;
    Camera mainCam;
    WeaponController weaponControl;
    ResourceDistributor distro;
    DamageHandler damageHandler;
	public bool drawGUI = true;
    
	// Use this for initialization
	void Start () {
        mainCam = GameHelper.getCamera();        
        player = GameHelper.getPlayer();
        collectibles = player.GetComponentInChildren<CollectibleHandler>();
        lineRender = gameObject.GetComponent<LineRenderer>();
        cursor = gameObject.GetComponent<MouseCursor>();
        weaponControl = player.GetComponentInChildren<WeaponController>();
        distro = player.GetComponentInChildren<ResourceDistributor>();
        damageHandler = player.GetComponentInChildren<DamageHandler>();

	}
    void Update()
    {   
        // render targeting line
        if (player != null)
        {
            lineRender.SetPosition(0, player.transform.position + player.transform.up * 2f);
            lineRender.SetPosition(1, player.transform.position + player.transform.up * 50f);
        }
        
    }
	
	// Update is called once per frame
    void OnGUI()	
	{
	if(drawGUI)
    {
        if (collectibles != null) GUI.Label(new Rect(10, 10, 300, Screen.height/2), collectibles.countText, style);
        if (damageHandler != null) GUI.Label(new Rect(Screen.width-310, 10, 300, 500), damageHandler.armorString,style);
        if (player != null) GUI.Label(new Rect(Screen.width / 2, 10, 200, 20), "Velocity: "+player.rigidbody2D.velocity.magnitude,style);
        if (mainCam != null) GUI.Label(new Rect(Screen.width / 2, 25, 200, 20), "Zoom Level: " + mainCam.GetComponent<CameraController>().zoomLevel, style);
        if ( player!= null) GUI.Label(new Rect(Screen.width/2, 40, 200, 20), "Ship Mass: " + player.rigidbody2D.mass,style);
        if (weaponControl != null) GUI.Label(new Rect(10f, Screen.height - 110, 300, 100), weaponControl.getCurrentWeaponGroupString(),style);
        if (distro != null) {
	    	Texture2D tex = (Texture2D) Resources.Load("Textures/fancyBar");
        	Texture2D tex2 = (Texture2D)Resources.Load("Textures/fancyBarRed2");
        	float energy = distro.getTotalQuantity(ResourceMeta.Type.Energy) / distro.getTotalCapacity(ResourceMeta.Type.Energy);
        	float w = (int)(Screen.width * 2/3/16)*16;
        	//Debug.Log(Screen.width+" "+Screen.width*2/3/8 + " " +w);
        	drawStatusBar(new Vector3(Screen.width / 2, 74f, 0f), 32f, w, tex2, tex, energy, 16);
		}
        if (damageHandler != null)
        {   
            Texture2D full = (Texture2D) Resources.Load("Textures/smallRedBar");
            Texture2D empty = (Texture2D) Resources.Load("Textures/smallBlackBar");
            float w = (int)(Screen.width * 2/3/16)*16;
            drawStatusBar(new Vector3(Screen.width / 2, 23f, 0f), 16f, w, empty, full, ((float)damageHandler.health / (float)damageHandler.healthMax));
            
        }
    }
	}

    public void drawStatusBar(Vector3 position, float height, float width, Texture2D back, Texture2D front, float percentage, float step = 0)
    {
        float newWidth;
        float oldWidth = width;
        if (step > 0)
        {
            newWidth = Mathf.RoundToInt(percentage * width / step) * step;
            //Debug.Log(newWidth);
            width = newWidth;
        }
        else
            width = percentage * width;
        Rect rBack = new Rect(position.x - oldWidth / 2 + width, Screen.height - position.y, oldWidth - width, height); 
        Rect rFront = new Rect(position.x - oldWidth / 2, Screen.height - position.y, width, height);

        if (back != null) GUI.DrawTextureWithTexCoords(rBack, back, new Rect(0, 0, (oldWidth - width) / back.width, height / back.height));
        if (front != null) GUI.DrawTextureWithTexCoords(rFront, front, new Rect(0, 0, width / front.width, height / front.height));
        //if (back != null) GUI.DrawTextureWithTexCoords(rBack, back, new Rect(0,0, (1-percentage)*width / back.width, height / back.height) );
        //if (front != null) GUI.DrawTextureWithTexCoords(rFront, front, new Rect(0,0, percentage*width / front.width, height / front.height) );
    }

    public void drawHealthBarAboveTarget(Transform target, Texture2D back, Texture2D front, float percentage)
    {        

        Vector3 extents;        // = ((SpriteRenderer)gameObject.renderer).sprite.bounds.extents;
        if (target.gameObject.renderer != null && target.gameObject.renderer.GetType() == typeof(SpriteRenderer))
        {
            SpriteRenderer spriterenderer = (SpriteRenderer) target.gameObject.renderer;
            extents = spriterenderer.sprite.bounds.extents;            
        }
        else
        {            
                extents = new Vector3(0f, 10f, 0f);
        }
            

        try
        {
            Vector3 position = mainCam.WorldToScreenPoint(target.position + new Vector3(0f, extents.y + 1f, -10f));
            drawStatusBar(position, healthBarHeight, healthBarWidth, back, front, percentage);            
        }
        catch (System.NullReferenceException e) { }
    }

	public void setGUI(bool b)
	{
		drawGUI = b;
	}
    
   
}
