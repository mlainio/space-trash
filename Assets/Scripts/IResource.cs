﻿using UnityEngine;
using System.Collections;
//using System.Collections.Generic;


public interface IResource
{
    int Quantity { get; set; }      //quantity is the amount of resources in usable units
    float Volume { get; set; }      //volume is the amount of resources in container capacity units
    //ResourceMeta.Type ResourceType { get; }
    ResourceMeta.Type ResourceType { get; }
    IResource createFromTemplate(ResourceMeta.ResourceTemplate template, float volume);
}