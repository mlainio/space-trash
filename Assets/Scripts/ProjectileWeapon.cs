﻿using UnityEngine;
using System.Collections;

public class ProjectileWeapon : Weapon, IShootable {

	
	public GameObject projectile;
	public GameObject spawnPoint;
    
    public float projectileVelocity = 1f;
	public float life = 1f;					//life of projectiles (seconds)
	public float shotsPerSec = 1;			//shots per second
	public float shotReloadTime = 0.5f;	//time (seconds) to reload after burst
	public float maxBurst = 3f;				//max number of projectiles in burst

    private float currentBurst = 0f;
	private float shootTimer=0f;
	private float reloadTimer=0f;
    public bool fire = false;

    public ResourceMeta.Type ammoType;
    public int ammoRate = 1;
    public ResourceContainer primaryContainer;

	// Use this for initialization
	public override void Start () {
        base.Start();
        state = State.Ready;
	}
	
	// Update is called once per frame
	void Update () {
                   
        if (shootTimer > 0)
        {
            shootTimer -= Time.deltaTime;
            state = State.Waiting;
        }
        else {
            state = State.Ready;
        }

        if (reloadTimer > 0)
        {
            reloadTimer -= Time.deltaTime;
            if (currentBurst >= maxBurst)
                state = State.Reloading;
        }
        else
        {
            currentBurst = 0;
            state = State.Ready;
        }
            //Debug.Log(state);
        if ((state == State.Ready || state == State.Firing) && fire)
        {
            if (distr != null)
                if (distr.require(ammoType, ammoRate, true, primaryContainer) == null)
                    state = State.OutOfAmmo;
        }
		if (fire && shootTimer <= 0 && state == State.Ready) {

            state = State.Firing;
            shoot();
            		
			currentBurst++;
			shootTimer=1/shotsPerSec;
			reloadTimer += shotReloadTime;            

		}
	
	}
    protected override void shoot()
    {
        float rotOff = Random.Range(-1 * accuracyModifier, accuracyModifier);

        Quaternion rotOffset = Quaternion.AngleAxis(rotOff, new Vector3(0, 0, 1));

        //create bullet at barrel mouth (nozzle)
        GameObject projectileClone = (GameObject)Instantiate(projectile, spawnPoint.transform.position, spawnPoint.transform.rotation * rotOffset);
        //MEthod not implemented yet in UNITY!

        //Physics.IgnoreCollision(projectile.collider2D, parentBody.gameObject.collider, true);        

        //apply bullet force to object
        if (parentBody != null)
            projectileClone.rigidbody2D.velocity = parentBody.velocity;

        //Vector2 dir = (Vector2) spawnPoint.transform.up * rotOffset;
        projectileClone.rigidbody2D.AddForce(projectileClone.transform.up * (projectileVelocity*projectileClone.rigidbody2D.mass));
        projectileClone.SendMessage("setToDestroy", life);
    }
    public void fireMain()
    {
        //Debug.Log("Shooting! "+gameObject.GetHashCode()+"/"+gameObject.GetType());
        fire = true;        
    }
    public void stopMain()
    {
        fire = false;
    }


    public void holdMain()
    {
        //throw new System.NotImplementedException();
    }

    public void fireAlt()
    {
        //throw new System.NotImplementedException();
    }

    public void stopAlt()
    {
        //throw new System.NotImplementedException();
    }

    public void holdAlt()
    {
        //throw new System.NotImplementedException();
    }
    //public override string ToString()
    //{
    //    return name + " " + this.state;
    //}

    public float effectiveRange
    {
        get { return projectileVelocity/100 * life; }
    }
}