﻿using UnityEngine;
using System.Collections;

public interface IShootable {
    //parameters
    float effectiveRange { get; }
    //methods
    void fireMain();
    void stopMain();
    void holdMain();
    void fireAlt();
    void stopAlt();
    void holdAlt();
}
