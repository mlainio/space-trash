﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ResourceDistributor : MonoBehaviour {

    private class Demand
    {
        public ResourceContainer container;        
        public ResourceMeta.Type resourceType;
        public int need;

        public Demand(ResourceContainer cont, ResourceMeta.Type type, int need)
        {
            this.container = cont;
            this.resourceType = type;
            this.need = need;
        }
    }
    
    private CollectibleHandler handler;
    private List<ResourceFactory> listFactories;
    private List<Demand> liDemand = new List<Demand>();
    

	// Use this for initialization
	void Start () {
	    try{
            handler = gameObject.GetComponentInChildren<CollectibleHandler>();
        }            
        catch (MissingComponentException e) {
            Debug.Log(e);
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}
    
    //use this method to use resources (E.g. engines and weapons use energy)
    public ResourceMeta require(ResourceMeta.Type resource, int quantity, bool allOrNothing = true, ResourceContainer priorityContainer = null)   
    {

        Queue<ResourceContainer> potentialContainers = new Queue<ResourceContainer>();
        int total = 0;
        if (priorityContainer != null)
            potentialContainers.Enqueue(priorityContainer);

        foreach (ResourceContainer rc in handler.getContainers())
        {
            if (rc.isLegalResource(resource) && rc.Quantity > 0 && rc != priorityContainer)
                potentialContainers.Enqueue(rc);

            total+= rc.Quantity;
        }

        //checks if there's enough to satisfy all required resources
        if (allOrNothing && total < quantity)
            return null;

        ResourceMeta supplied = new ResourceMeta();
        supplied.type = resource;        
        while (potentialContainers.Count > 0 && quantity > 0 && potentialContainers.Peek() != null)
        {
            // this loop iterates through all potential containers (valid resource type)
            // puts them in queue and takes as much as possible from each container
             
            try
            {                   
                ResourceContainer temp = potentialContainers.Dequeue();
                ResourceMeta removed = (ResourceMeta)temp.removeResource(resource, quantity);
                supplied.Quantity += removed.Quantity;
                quantity -= supplied.Quantity;
                setDemand(temp, resource, supplied.Quantity);

            }
            catch (System.NullReferenceException e)
            {
                //Debug.Log(e);
            }
            
        }
        if (supplied.Quantity == 0)
        {
            return null;
        }        

        handler.resourceUpdateRequest();
        return supplied;
                
    }
    public int getTotalQuantity(ResourceMeta.Type resource)
    {
        int total = 0;
        foreach (ResourceContainer rc in handler.getContainers())
        {                            
            if (rc.isLegalResource(resource))
                total+= rc.Quantity;
        }

        return total;
    }
    public float getTotalCapacity(ResourceMeta.Type resource)
    {
        float total = 0;
        foreach (ResourceContainer rc in handler.getContainers())
        {
            if (rc.isLegalResource(resource))
                total += rc.capacity;
        }

        return total;
    }
    //Factories call this method to give the distributor resources to distribute among all valid containers
    public void supply(ResourceMeta production)
    {
        foreach (ResourceContainer rc in handler.getContainers())
        {
            int supplied = 0;
            //fetch the first container that has space for said resource
            if (rc.isLegalResource(production.ResourceType))
            {
                if (rc.freeCapacity() > 0)
                {
                    //add resources to said container
                    if (rc.hasRoomFor(production))
                    {
                        supplied = production.Quantity;
                        rc.addResource(production, production.Quantity);
                        //Debug.Log("Supplied Full production");
                    }
                    //if only partial space, add rest to another.
                    else
                    {
                        ResourceMeta alteredProduction = (ResourceMeta)production.createFromTemplate(ResourceMeta.getTemplate(production.ResourceType),0f);
                        alteredProduction.Volume = rc.freeCapacity();
                        rc.addResource(alteredProduction, alteredProduction.Quantity);
                        supplied = alteredProduction.Quantity;
                        production.Quantity -= alteredProduction.Quantity;
                        //Debug.Log("Supplied partial production");

                    }
                        
                    //if a container became full, reduce demand
                    if (ResourceMeta.volumeToQuantity(production.ResourceType, rc.freeCapacity())  <= 0) {                        
                        //Debug.Log("Demand for "+production.ResourceType+": "+ResourceMeta.typeIndex(production.ResourceType));
                    }
                    handler.resourceUpdateRequest();
                    setDemand(rc, production.ResourceType, supplied * -1);
                }            
            }
        }               
    }
    //private void distribute(ResourceMeta production) {}
    
    // this function sets the demand for a particular resource.
    //As long as there's demand, distributor will order factories of said type to produce the resource
    public void setDemand(ResourceContainer cont, ResourceMeta.Type type, int need)
    {
        int hits = 0;
        //iterate through list and count hits, then add as new if no hits (match not found)
        foreach (Demand d in liDemand)
        {
            if (d.container == cont)
                if (d.resourceType == type) {
                    d.need += need;
                    hits++;           
                }                    
        }
        if (hits == 0)
            liDemand.Add(new Demand(cont, type, need));
        
    }
    public int checkDemand(ResourceMeta.Type resource)
    {
        int need = 0;
        foreach (Demand d in liDemand)
        {
            if ( d.need > 0 && d.resourceType == resource )
                need += d.need;
        }
        return need;
    }
}
