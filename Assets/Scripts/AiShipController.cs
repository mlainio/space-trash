﻿using UnityEngine;
using System.Collections;

public class AiShipController : ShipController
{
    public float minAngleToAccelerate = 1f;
    enum axis { vertical, horizontal };
    
    public float desiredDistance = 5f;
    public float combatSpeed = 10f;
    public string enemyTag;
    public string patrolTag;
    public enum AiState { Roam, Patrol, Combat, Flee };    
    public AiState currentState = AiState.Patrol;
    AiState oldState;    
    public GameObject patrolTarget;
    public bool strafeRight = true;
    public float strafeDistance = 30f;
    public float roamRadius = 64f;
    public float strafePchangeDirection = 0.01f;

    TrackObject objectTracker;
    WeaponController weaponController;
    ObjectScanner scanner;
    
    //timers
    Timer strafeTimer = new Timer(2f, true);
    Timer roamTimer = new Timer(30f, true);
    public float PrandomActOfViolence = 0.001f;

    public override void Start()
    {        
        base.Start();
        //ai = new AiInput();
        objectTracker = gameObject.GetComponent<TrackObject>();
        weaponController = gameObject.GetComponentInChildren<WeaponController>();
        scanner = gameObject.GetComponent<ObjectScanner>();
        
        //sets the ai to patrol on start        
    }
    void Update()
    {
        strafeTimer.Update(Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.F))
        {
            weaponController.setWeaponGroupMode(WeaponController.WeaponMode.FireMain);
        }
        //*** AI LOGIC ***        
        //Check for patrol target

        if (patrolTarget == null)
            seekPatrolTarget();
        //seek hostile target
        if (currentState == AiState.Roam || !objectTracker.hasTarget)
        {
            seekHostiles();
        }
        //Roaming...
        if (!objectTracker.hasTarget || currentState == AiState.Roam && roamTimer.isFinished())
        {
            if (currentState == AiState.Patrol && patrolTarget == null)
                currentState = AiState.Roam;

            float rand = Random.Range(0f, 1f);
            if (rand < PrandomActOfViolence)
            {
                objectTracker.addTarget(scanner.getNearestObject());
                oldState = currentState;
                currentState = AiState.Combat;
            }
            else
            {
                currentState = oldState;
            }
        }
        else { roamTimer.Update(Time.deltaTime); }

        //DETERMINE ACTION
        switch (currentState)
        {
            case AiState.Combat:
                if (objectTracker.VectorToTarget.magnitude <= weaponController.effectiveRange)
                {
                    weaponController.setWeaponGroupMode(WeaponController.WeaponMode.FireMain);
                }
                else
                {
                    weaponController.setWeaponGroupMode(WeaponController.WeaponMode.StopMain);
                }
                break;
            case AiState.Roam:                
                roam(transform.position);
                                
                break;
            case AiState.Patrol:
                {
                    if (!seekHostiles())
                    {
                        roam(patrolTarget.transform.position);
                    }
                }
                break;
            case AiState.Flee:
            
            default:
                break;
        }
    }
    //
    void roam(Vector2 position)
    {
        if (!roamTimer.isActive() || objectTracker.VectorToTarget.magnitude <= desiredDistance * 1.10f)
        {
            roamTimer.reset();
            objectTracker.addNavPoint(position, roamRadius);
        }
    }
    bool seekHostiles()
    {
        GameObject nearestHostile = scanner.getNearestObjectWithTag(enemyTag);
        if (nearestHostile != null)
        {
            objectTracker.addTarget(nearestHostile);
            oldState = currentState;
            currentState = AiState.Combat;
            return true;
        }

        return false;
    }
    void seekPatrolTarget() {
        patrolTarget = scanner.getNearestObjectWithTag(patrolTag);
        if (patrolTarget != null)
            currentState = AiState.Patrol;
    }
    protected override int getMoveInput()
    {
        // Does AI consume fuel? Perhaps not.
        return 0;
    }
    public override Vector2 getForceVector()
    {
        //combat state
        //Raw force fector = normalized targetvector multiplied by moveforce adjusted by the force of current velocity
        Vector2 rawVector = (objectTracker.VectorToTarget.normalized*moveForce) - (velocityTracker.velocity / rigidbody2D.mass);
        
        // move force vector is clamped by max move force
        Vector2 clampedForceVector = Vector2.ClampMagnitude(rawVector, moveForce);
        Vector2 velocityToDesiredDistance = objectTracker.VectorToTarget - objectTracker.VectorToTarget.normalized * desiredDistance;
                        
        // to calculate the brake distance we first need acceleration
        float acceleration = moveForce / rigidbody2D.mass;
        float brakedistance = (-Mathf.Pow(velocityTracker.velocity.magnitude, 2) / 2 * -acceleration);

        //let's clamp the vector by how close we are
        Vector2 finalvector = new Vector2();
        finalvector = Vector2.ClampMagnitude((velocityToDesiredDistance - velocityTracker.velocity * 1.5f) * rigidbody2D.mass, moveForce);
        Vector2 predictedVelocity = finalvector / rigidbody2D.mass + velocityTracker.velocity;
        
        //strafe when in combat
        if (currentState == AiState.Combat)
        {
            if (predictedVelocity.magnitude < combatSpeed && objectTracker.VectorToTarget.magnitude <= strafeDistance)
            {
                finalvector = finalvector + getStrafeSpeed();
                finalvector = Vector2.ClampMagnitude(finalvector, moveForce);
            }
        }

        //collision avoidance

        GameObject obstacle = scanner.getProbableCollision(finalvector.normalized, 32f);
        if (obstacle != null)
        {
            float distance = Vector2.Distance(transform.position, obstacle.transform.position);
            float evadeDistance = desiredDistance * 4;
            if (distance <= evadeDistance)
            {
                Vector3 direction = (obstacle.transform.position - transform.position);
                //Vector3 evadeDirection = Vector3.Cross(Vector3.up, direction);
                Vector2 evadeDirection = new Vector2(direction.normalized.x, direction.normalized.y*-1);
                finalvector += (Vector2)evadeDirection * moveForce;
                //Debug.Log("obstacle in direction " + direction + "evade vector " + evadeDirection);

            }
        }
        forceVector = Vector2.ClampMagnitude(finalvector, moveForce);
        return forceVector;
    }

    Vector2 getStrafeSpeed()
    {
        Vector2 strafeVector = new Vector2();
        if (strafeTimer.isFinished())        {           

            if (Random.Range(0f, 1f) < strafePchangeDirection)
            {
                strafeRight = !strafeRight;
            }
            strafeTimer.reset();
        }
        if (strafeRight)
            strafeVector = transform.right;
        else
            strafeVector = transform.right * -1;

        return strafeVector * moveForce * strafeMultiplier;
        
    }
    
    
}
