﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class WeaponController : MonoBehaviour {
    public enum WeaponMode { FireMain, StopMain, HoldMain, FireAlt, StopAlt, HoldAlt };
    public GameObject defaultWeapon;
    public List<WeaponGroup> listWeaponGroup = new List<WeaponGroup>();
    public Rigidbody2D parentBody;
    private int activeGroup = 0;
    public int numWepGroups = 2;
    private List<Transform> listSlots = new List<Transform>();

	// Use this for initialization
    void Awake() {
        for (int i = 0; i < numWepGroups; i++)
        {
            listWeaponGroup.Add(new WeaponGroup(i));       //add proper number of weaponGroups
            
        }
    }
	void Start () {

        getSlots();         //Find weapon slots attached to the gameobject

        foreach (Transform slot in listSlots)
        {
            slot.gameObject.GetComponent<WeaponSlot>().activate();      //tells the slot to wake up and create assigned weapons
        }
 
	}

    /*
     * Create Weapon at position and assign it to this Controller
     */
    public GameObject createWeapon(GameObject weapon, Transform owner, WeaponController controller)
    {
        GameObject clone;
        clone = (GameObject)GameObject.Instantiate(weapon, owner.position, transform.rotation);
        clone.transform.parent = owner;
        clone.GetComponent<Weapon>().controller = controller;        
        owner.gameObject.GetComponent<SpriteRenderer>().enabled = false;

        return clone;
    }
	
	// Update is called once per frame
    void getSlots()
    {        
        Transform[] array = GetComponentsInChildren<Transform>();
        foreach (Transform t in array)
        {
            if (t.gameObject.tag == "Slot")
            {
                listSlots.Add(t);
            }
        }
        
    }
    public void setWeaponGroupMode(WeaponMode mode)
    {
        listWeaponGroup[activeGroup].setMode(mode);
    }
    void Update () {
   
       if(Time.timeScale != 0){ 
        
        updateWeaponGroups();
	   }// timeScale
	}

private void updateWeaponGroups()
{
    foreach (WeaponGroup group in listWeaponGroup)
    {
        group.Update();
    }
}
    public bool setWeaponGroup(int activeGroup)
    {   
        if (listWeaponGroup[activeGroup] == null)
             return false;
        
        this.activeGroup = activeGroup;
        return true;
        
    }

    public void addWeaponToGroup(IShootable weapon, int weaponGroup)
    {        
        if (weaponGroup >= numWepGroups)
            return;

        if (listWeaponGroup[weaponGroup].addItem(weapon))
        {
            Debug.Log("Weapon added successfully to group " + weaponGroup, gameObject);
        }
        
    }
	public void removeWeaponFromGroup(IShootable wpn, int weaponGroup)
	{
		if(weaponGroup >= numWepGroups)
			return;
		if(listWeaponGroup[weaponGroup].deleteItem(wpn))
		{
			Debug.Log("Weapon removed successfully from group " + weaponGroup, gameObject);
		}
	}

	//Dublikaatti tällä hetkellä
	public void UpdateNewWeaponToGroup(IShootable weapon, int weaponGroup)
	{   
		if (weaponGroup >= numWepGroups)
			return;
		
		if (listWeaponGroup[weaponGroup].addItem(weapon))
		{
			Debug.Log("Weapon added successfully to group " + weaponGroup, gameObject);
		}
		
	}
    public string getCurrentWeaponGroupString()
    {
        return listWeaponGroup[activeGroup].ToString();
    }

	public List<Transform> getSlotslist()
	{
		return listSlots;
	}
	public List<WeaponGroup> getlistWeaponGroup()
	{
		return listWeaponGroup;
	}
	public WeaponGroup getWeaponGroup(int i)
	{
		return listWeaponGroup[i];
	}


    public void setActiveGroup(int p)
    {
        if (p >= 0 && p < numWepGroups)
        {
            activeGroup = p;
        }
    }

    public float effectiveRange
    {
        get
        {
            //if (listWeaponGroup[activeGroup])
            float er = 0;
            foreach (IShootable w in listWeaponGroup[activeGroup].getlistWeapon())
            {
                er += w.effectiveRange;
            }
            er = er / listWeaponGroup[activeGroup].getlistWeapon().Count;
            return er;
        }
    }
}
