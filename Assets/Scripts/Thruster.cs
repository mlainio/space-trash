﻿using UnityEngine;
using System.Collections;

public class Thruster : MonoBehaviour {
	ParticleSystem thrustParticles;
    private float delay = 0.2f;
    private float delaytimer = 0;
	public AudioSource loopSound;
    bool halted = false;
	public float audioMultiplier = 1f;
	public float audioRangeMultiplier = 1f;
    
	public enum Position{
		Main, Turbo, Left, Right, FrLeft, FrRight, AftLeft, AftRight, Total
	};
	public Position position;

	// Use this for initialization
	void Start () {
		thrustParticles = this.particleSystem;
		thrustParticles.Stop();
		loopSound.volume = GameHelper.getAudioController().getVolume(AudioController.audioType.Thrusters) * audioMultiplier;
		loopSound.maxDistance = GameHelper.getAudioController().getRange(AudioController.audioType.Thrusters) * audioRangeMultiplier;
	}
	
	// Update is called once per frame
	void Update () {
        if (delaytimer > 0)
            delaytimer -= Time.deltaTime;
	}

	public void Thrust (Position direction) {
        if (direction == position && !halted && delaytimer <= 0)
        {
            if (!thrustParticles.isPlaying) thrustParticles.Play();
            delaytimer = delay;
			if (loopSound != null && !loopSound.isPlaying) loopSound.Play();
        }
		
            
	}
    public void Thrust()
    {
        if (!thrustParticles.isPlaying && delaytimer <= 0)
        {
            thrustParticles.Play();
            delaytimer = delay;
			
			
        }
		
    }

	public void Cease (Position direction) {
		if (direction == position && delaytimer <= 0) {
			thrustParticles.Stop ();
            delaytimer = delay;
			if (loopSound != null && loopSound.isPlaying) loopSound.Stop();
		}

	}
    public void Cease()
    {
        if (delaytimer <= 0)
        {
            thrustParticles.Stop();
            delaytimer = delay;
        }

            
    }
    public void Halt(Position direction)
    {
        if (direction == position)
        {            
            thrustParticles.Stop();
            halted = true;
        }
    }
    public void Release()
    {
        if (halted)
        {            
            thrustParticles.Play();
            halted = false;
        }
            
        
    }
}
