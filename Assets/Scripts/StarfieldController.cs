﻿using UnityEngine;
using System.Collections;

public class StarfieldController : MonoBehaviour {
    public float speedPower = 2.0f;
    public float speedMultiplier = 0.1f;
    Vector3 baseScale;
    Camera mainCam;
    CameraController control;
    VelocityTracker tracker;
    public float velocityThreshold = 10f;
    
    float oldSize;
    private float oldZoom;
    private float zoomScale;
    //float oldLife;
	// Use this for initialization
	void Start () {        
        particleSystem.Play();        
        mainCam = GameHelper.getCamera();
        //tracker = mainCam.gameObject.GetComponent<VelocityTracker>();
        tracker = GameHelper.getPlayer().GetComponent<VelocityTracker>();
        Debug.Log(tracker);
            //GameHelper.getPlayer().GetComponent<PlayerShipController>();
        control = mainCam.gameObject.GetComponent<CameraController>();
        Debug.Log(control);
        baseScale = particleSystem.transform.localScale;
        oldZoom = mainCam.orthographicSize;
        oldSize = particleSystem.startSize;
        //oldLife = particleSystem.startLifetime;
	}
	
	// Update is called once per frame
    void Update()
    {
        //Adjusts particlesystem scale according to zoom level!

        if (mainCam.orthographicSize > baseScale.x * 6)
        {
            zoomScale = (mainCam.orthographicSize / oldZoom) * 0.8f +0.2f;
            float emitScale = mainCam.orthographicSize * 0.5f;
            particleSystem.transform.localScale = new Vector3(1 + emitScale, 1 + emitScale, 1f);
            //float fuck = particleSystem.startSize;
            particleSystem.startSize = oldSize * zoomScale;
            //particleSystem.startLifetime = oldLife * zoomScale;
           // particleSystem. *= zoomScale;
        }
        else
        {
            particleSystem.transform.localScale = baseScale*6;
        }
        //oldZoom = mainCam.orthographicSize;

            

        //if (player.getMoveVector().magnitude > 0.1f)
        //{
        //    particleSystem.Play();
        //}

        
        
    }
    void LateUpdate()
    {

        ParticleSystem.Particle[] p = new ParticleSystem.Particle[particleSystem.particleCount + 1];        
        //float zoomScale = oldZoom;
        
        //Direction of move
        Vector2 move = tracker.velocityNow;

        //Debug.Log(tracker.velocityNow + " " + tracker.averageVelocity() + " "+ move);

        if (move.magnitude < velocityThreshold)
            move = move * 0;
        else
            move = (move.magnitude - velocityThreshold) * move.normalized;
        
        float speed = Mathf.Pow(tracker.velocityNow.magnitude * speedMultiplier, speedPower);

        int l = particleSystem.GetParticles(p);

        int i = 0;
        while (i < l)
        {
            
            
            p[i].velocity = ((Vector3) move.normalized*-speed)*zoomScale;
            i++;
            //if (zoomScale != oldZoom)
            //{
            //    p[i].size *= Mathf.Pow(zoomScale, 2);                
            //}
                
            
        }


        particleSystem.SetParticles(p, l);
        //particleSystem.playbackSpeed =         
    }
}
