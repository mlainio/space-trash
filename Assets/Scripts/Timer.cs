﻿using System;
using UnityEngine;
using System.Collections.Generic;
/*
 * Generic Timer Class
 * Use constructor to set time and activate
 * otherwise use Set-method to activate
 * call Timer.Update(time) on Unity Update. It will reduce timer value by time
 *  
 * isFinished can be used to see if timer has finished its cycle. Usable only once
 * isActive can be used whenever to see if timer is active or not
 * 
*/
public class Timer
{
    //TODO: Redo with enums and parameters
    //enum State { Disabled, Active, Running, Finished };
    //State timerState;
    float timeleft;
    float defaultTime;
    bool active;
    bool finished = false;
    public Timer(float time, bool active)
    {
        defaultTime = time;
        this.active = active;
        if (active)
            timeleft = time;
    }
    public void Update(float deltaTime) {
        if (active)
        {
            timeleft -= deltaTime;
            if (timeleft <= 0)
            {
                active = false;
                finished = true;
            }
                
        }
    }
    public void set(float time)
    {
        timeleft = time;
        active = true;
        finished = false;
    }
    public void reset()
    {
        set(defaultTime);
    }
    public bool isFinished()
    {
        if (finished)
        {
            finished = false;
            return true;
        }
        else return false;
    }
    public bool isActive()
    {
        return active;
    }
}
