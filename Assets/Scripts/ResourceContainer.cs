﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
/*
 * ResourceContainer contains bunch of resources. 
 *
 */
//[RequireComponent(typeof(Rigidbody2D))]
public class ResourceContainer : MonoBehaviour
{
        
    private static int nextId = 1;
    private int id=0;
    public enum LegalType { Energy, Supplies, Crew, Ordnance, Cargo };
    public LegalType legalType;
    private ResourceMeta.Type actualType;
    private List<IResource> containedResources = new List<IResource>();
    public float capacity;      //capacity in Volume units
    private float selfMass;     //object's original mass (without the contents)
    public bool startFull = false;    
    

    //public ResourceMeta.Type legalTypes;
    // Use this for initialization
    void Awake()
    {
        id = nextId;
        nextId++;
        switch (legalType)
        {
            case LegalType.Energy: actualType = ResourceMeta.Type.Energy;
                break;
            case LegalType.Supplies: actualType = ResourceMeta.Type.Supplies;
                break;
            case LegalType.Crew: actualType = ResourceMeta.Type.Crew;
                break;
            case LegalType.Ordnance: actualType = ResourceMeta.Type.Ordnance;
                break;
            case LegalType.Cargo: actualType = ResourceMeta.Type.Crystal | ResourceMeta.Type.Mineral | ResourceMeta.Type.RareMineral | ResourceMeta.Type.ExoticMineral;
                break;
            default:
                break;
        }
        if (gameObject.GetComponent<Rigidbody2D>() == null)
            selfMass = 0;
        else
            selfMass = rigidbody2D.mass;

    }
    void Start()
    {
        if (startFull)
        {
            ResourceMeta.ResourceTemplate temp = ResourceMeta.getTemplate(actualType);
            switch (actualType)
            {
                case ResourceMeta.Type.Energy:
                    
                    IResource core = new ResourceMeta();
                    core = core.createFromTemplate(temp, capacity);
                    addResource(core, ResourceMeta.volumeToQuantity(core.ResourceType, capacity));
                    Debug.Log(actualType + " starting full: " + this.ToString());
                    transform.parent.gameObject.BroadcastMessage("SetCountText", SendMessageOptions.DontRequireReceiver);
                    transform.parent.gameObject.BroadcastMessage("updateParentMass", SendMessageOptions.DontRequireReceiver);
                    break;
                //case ResourceMeta.Type.Ordnance:
                    
                //    IResource ammo = new ResourceMeta();
                //    ammo = ammo.createFromTemplate(temp, capacity);
                //    addResource(ammo, (int)capacity);
                //    Debug.Log(actualType + " starting full: " + this.ToString());
                //    gameObject.SendMessageUpwards("SetCountText");
                //    gameObject.SendMessageUpwards("updateParentMass");
                //    break;
                default:
                    IResource res = new ResourceMeta();
                    res = res.createFromTemplate(temp, capacity);
                    addResource(res, ResourceMeta.volumeToQuantity(res.ResourceType, capacity));
                    Debug.Log(actualType + " starting full: " + this.ToString());
                    transform.parent.gameObject.BroadcastMessage("SetCountText", SendMessageOptions.DontRequireReceiver);
                    transform.parent.gameObject.BroadcastMessage("updateParentMass", SendMessageOptions.DontRequireReceiver);
                    break;
            }
        } 
       
            
    }

    // Update is called once per frame
    void Update()
    {

    }
    public int Quantity
    {
        get
        {
            int qt = 0;
            foreach (IResource ru in containedResources)
            {
                qt += ru.Quantity;
            }
            return qt;
        }
    }
    public float mass
    {
        get
        {
            float vol = 0;
            foreach (IResource ru in containedResources)
            {
                vol += ru.Quantity*ResourceMeta.getTemplate(ru.ResourceType).MassPerUnit;   //can't use volume because volume isn't necessarily mass!!
            }
            return vol+selfMass;
        }
    }
    public float Volume
    {
        get
        {
            float vol = 0;
            foreach (IResource ru in containedResources)
            {
                vol += ru.Volume;
            }
            return vol;
        }
    }
    //conversion to quantity needs to be made per resource
    public float freeCapacity(bool percentage = false)
    {        
        float n = capacity - Volume;
        if (percentage)
            return n / capacity;
        
        return n;
    }
    //readonly total worth of container (value * mass of every item)
 
    public bool addResource(IResource res, int quantity)
    {
        bool added = false;
        if (isLegalResource(res.ResourceType))
        {            
           
            foreach (IResource ru in containedResources)
            {
                if (ru.ResourceType == res.ResourceType)
                {
                    ru.Quantity += quantity;
                    added = true;
                }
                
                
            }
            if (!added)     //container does not contain any resources
            {
                IResource newres= res.createFromTemplate(ResourceMeta.getTemplate(res.ResourceType),0f);
                //Debug.Log(res.ResourceType + "2");
                newres.Quantity = quantity;
                //Debug.Log("new volume: "+ newres.Volume);
                containedResources.Add(newres);
                added = true;
            }
        }
        //Debug.Log("Added resource: " + added);
        return added;

    }

    //removes units units of resource res and returns the removed resources or null if operation wasn't successful
    public IResource removeResource(ResourceMeta.Type res, int units)
    {
        
        if (!isLegalResource(res))
            return null;       
        
        IResource removed = null;       //default: give nothing
        List<IResource> toBeRemoved = new List<IResource>();
        
        if (containedResources.Count > 0)
            foreach (IResource ru in containedResources)
            if (ru.ResourceType == res)
                if (units > ru.Quantity) {                    
                    //give everything you have
                    removed = new ResourceMeta();
                    removed = removed.createFromTemplate(ResourceMeta.getTemplate(ru.ResourceType), 0f);
                    removed.Quantity = ru.Quantity;
                    toBeRemoved.Add(ru);
                }
                else {   
                    // give the proper amount
                    removed = new ResourceMeta();
                    removed = removed.createFromTemplate(ResourceMeta.getTemplate(ru.ResourceType), 0f);
                    ru.Quantity -= units;
                    removed.Quantity += units;
                }
        containedResources.RemoveAll(t => toBeRemoved.Contains(t));
        return removed;
     }    

    public bool isLegalResource(ResourceMeta.Type type)
    {
        bool var = (((actualType & type) == type));
        //Debug.Log("Check: " + type + "(" + actualType + ")="+var);
        return var;
        //Logiikkaa... (realLegal U actualType) = actualType...
        //return (((realLegal & type) == type));
    }

    public bool hasRoomFor(IResource resource)
    {
        //Debug.Log(resource.GetHashCode() + "/" + resource.GetType()+"/"+resource==null);
        float temp = (capacity -(this.Volume + resource.Volume));
        //Debug.Log("hasRoomFor: " + temp + " ("+this.Volume+"/" + capacity+")");
        return temp >= 0;
    }    
    public override string ToString()
    {
        string s = legalType + " container " + gameObject.name +" "+id+" ["+Mathf.RoundToInt((1-freeCapacity(true))*100)+"%]"+"\n";
        foreach (IResource ru in containedResources)
        {
            s += "\t-"+ru + "\n";
        }
        return s;
    }
}
