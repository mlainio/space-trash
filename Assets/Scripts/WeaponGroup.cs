﻿using UnityEngine;

using System.Collections;
using System.Collections.Generic;

public class WeaponGroup {
    List<IShootable> listWeapon;
    int groupIndex;
    int groupSize = 10;
    bool linked = true;
    int selected = 0;
    float linkedShootTimer = 0;
    float linkedShootInterval = 0.25f;
    WeaponController.WeaponMode mode = WeaponController.WeaponMode.StopMain;
    
	// Use this for initialization    
    public void Update()
    {
        //Debug.Log(this.GetHashCode());
    }
    public WeaponGroup(int i)
    {
        groupIndex = i;
        Debug.Log("Created " + GetHashCode() +" "+ GetType().ToString());
        listWeapon = new List<IShootable>(groupSize);
    }
    private void next()
    {   
        selected++;
        if (selected >= groupSize || listWeapon[selected] == null)
            selected = 0;          
    }
    public bool addItem(IShootable weapon)
    {
        //if (listWeapon.Count >= groupSize)
        //    return false;
        //else
        //{   
            Debug.Log("Adding weapon "+weapon.GetHashCode()+" to "+GetHashCode());
            listWeapon.Add(weapon);
            Debug.Log("Now has "+listWeapon.Count+" items");
            return true;
        //}
    }
	public bool deleteItem(IShootable wpn)
	{
		Debug.Log("Removing weapon "+wpn.GetHashCode()+" from "+GetHashCode());
		string st = wpn.ToString();
		Debug.Log("Given remove param: "+wpn);
		/*string tmp;
		tmp = st.Substring(st.IndexOf(' '));
		st = st.Substring(0,st.Length - tmp.Length);
		st = st+"(Clone)";
		st = st + tmp;
		Debug.Log("Given remove param st: "+st);*/
		Debug.Log("First item in the list: "+listWeapon[0]);
		listWeapon.Remove(wpn);
		Debug.Log("Now has "+listWeapon.Count+" items");
		return true;
	}

	/*public bool addItem(IShootable weapon,int x)
	{
 		Debug.Log("Adding weapon "+weapon.GetHashCode()+" to "+GetHashCode());
		//listWeapon=null;
		listWeapon[x] = weapon;
		Debug.Log("Now has "+listWeapon.Count+" items");
		return true;

	}

	public void changeWeapon(GameObject wpn, int i){
		listWeapon[i] = wpn;
	}
	*/
    public void setMode( WeaponController.WeaponMode mode)
    {
        this.mode = mode;
        linkedAction();
    }
    //public bool removeItem(IShootable weapon) {}
    private void linkedAction()
    {
        foreach (IShootable weapon in listWeapon)
        {
            //Debug.Log("Set Weapon mode: " + mode.ToString()) + "/" + weapon.GetHashCode());
            switch (mode)
            {
                case WeaponController.WeaponMode.FireMain:
                    //Debug.Log("Set Weapon mode: " + mode.ToString()+"/"+weapon.GetHashCode());
                    weapon.fireMain();
                    break;
                case WeaponController.WeaponMode.StopMain:
                    weapon.stopMain();
                    break;
                case WeaponController.WeaponMode.HoldMain:
                    weapon.holdMain();
                    break;
                case WeaponController.WeaponMode.FireAlt:
                    weapon.fireAlt();
                    break;
                case WeaponController.WeaponMode.StopAlt:
                    weapon.stopAlt();
                    break;
                case WeaponController.WeaponMode.HoldAlt:
                    weapon.holdAlt();
                    break;
                default:
                    break;
            }
        }
    }
    
    public override string ToString()
    {
        string wpn = "Weapon Group "+groupIndex +" - " + ((linked) ? "Linked" : "Alternating")+"\n";
        foreach (IShootable weapon in listWeapon)
        {
            wpn += weapon.ToString()+"\n";
        }
        return wpn;
    }

	public List<IShootable> getlistWeapon()
	{
		return listWeapon;
	}

	public void changeWeapon(IShootable wpn, int i)
	{
		listWeapon[i] = wpn;
	}
	    
}
