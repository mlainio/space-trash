﻿using UnityEngine;
using System.Collections;

//[RequireComponent (typeof (VelocityTracker))]
public class DamageDealer : MonoBehaviour {
    [SerializeField]
    public Damage damage = new Damage(Damage.Type.Kinetic,1);
    VelocityTracker tracker;
	// Use this for initialization
	void Start () {
        tracker = GetComponent<VelocityTracker>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnCollisionEnter2D (Collision2D other)
    {
        if (other.gameObject.layer != LayerMask.NameToLayer("IgnoreCollision")) {
            
            //if we find an object on orbit, we want to break orbit and damage ourselves!
            SimpleOrbit orbit = other.gameObject.GetComponentInChildren<SimpleOrbit>();
            if (orbit == null)
                if (other.transform.parent != null) 
                    orbit = other.transform.parent.gameObject.GetComponentInChildren<SimpleOrbit>();

            if (orbit != null)
            {
                Debug.Log("Found Orbit");
                if (orbit.breakOrbit())
                {
                    DamageDealer odd = other.gameObject.GetComponent<DamageDealer>();
                    if (odd == null)
                        odd = other.transform.parent.gameObject.GetComponent<DamageDealer>();

                    if (odd != null)
                        odd.damageOther(this.gameObject);
                }

                
            }
            //Debug.Log(gameObject.tag + " dealing damage to " + other.gameObject.tag);
            damageOther(other.gameObject);
        }
        
    }
    public void damageOther(GameObject other, bool damageByDistance = false, float radius = 0)
    {
        //Debug.Log(LayerMask.LayerToName(other.layer));
        Damage dmg;
        GameObject otherObject;
        // If damagehandler is not found, let's try the parent
        if (other.GetComponent<DamageHandler>() == null)
            otherObject = other.transform.parent.gameObject;
        else
            otherObject = other;

        //if damagehandler still not found, skip
        if (otherObject.GetComponent<DamageHandler>() == null)
        {
            Debug.Log("DamageHandler Not Found!");
            return;
        }

        switch (damage.type)
        {
            case Damage.Type.Kinetic:
                float tempDamage = 0;
                Vector2 velocity = new Vector2();
                Vector2 otherVelocity = new Vector2();
                if (tracker != null)
                {
                    velocity = tracker.averageVelocity();
                    otherVelocity = otherObject.transform.gameObject.GetComponent<VelocityTracker>().averageVelocity();
                    
                    tempDamage = Damage.kinetic(velocity, otherVelocity, rigidbody2D.mass, otherObject.gameObject.rigidbody2D.mass, this.damage);

                }
                dmg = new Damage(damage.type, (int)tempDamage, damage.penetration);
                break;
            default:
                dmg = new Damage(damage.type, Damage.basic(damage), damage.penetration, damage.effectVector);
                break;
        }
        string mylayer = LayerMask.LayerToName(gameObject.layer);
        //string otherlayer = LayerMask.LayerToName(other.layer);
        //Debug.Log(mylayer+" dealing "+dmg.value+" "+ dmg.type+" damage to "+otherlayer);
        Debug.Log(gameObject.tag + " dealing " + dmg.value + " " + dmg.type + " damage to " + other.gameObject.tag);
        

        if (damageByDistance)
        {
            float distance = Vector2.Distance((Vector2)transform.position, (Vector2)otherObject.transform.position);
            float distanceMult = 1 - (distance / radius);
            if (distanceMult > 0f)
            {
                dmg.value = Mathf.RoundToInt(dmg.value * distanceMult);
            }
            else dmg.value = 0;

        }
        otherObject.SendMessage("applyDamage", dmg, SendMessageOptions.DontRequireReceiver);
        if (mylayer == "Projectile")
            gameObject.GetComponent<DamageHandler>().applyDamage(new Damage(Damage.Type.Heat, 999f));
    }
}
