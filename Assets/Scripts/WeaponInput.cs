﻿using UnityEngine;
using System.Collections;

public class WeaponInput : MonoBehaviour {

    WeaponController weaponController;
	// Use this for initialization
	void Start () {
        weaponController = gameObject.GetComponent<WeaponController>();
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.timeScale != 0)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                weaponController.setWeaponGroupMode(WeaponController.WeaponMode.FireMain);
            }
            if (Input.GetButtonUp("Fire1"))
            {
                weaponController.setWeaponGroupMode(WeaponController.WeaponMode.StopMain);
            }
            if (Input.GetButton("Fire1"))
            {
                weaponController.setWeaponGroupMode(WeaponController.WeaponMode.HoldMain);
            }
            if (Input.GetButtonDown("Fire2"))
            {
                weaponController.setWeaponGroupMode(WeaponController.WeaponMode.FireAlt);
            }
            if (Input.GetButtonUp("Fire2"))
            {
                weaponController.setWeaponGroupMode(WeaponController.WeaponMode.StopAlt);
            }
            if (Input.GetButton("Fire2"))
            {
                weaponController.setWeaponGroupMode(WeaponController.WeaponMode.HoldAlt);
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                weaponController.setActiveGroup(0);
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                weaponController.setActiveGroup(1);
            }
            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                weaponController.setActiveGroup(2);
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                weaponController.setActiveGroup(3);
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                weaponController.setActiveGroup(4);
            }
        }
	
	}
}
