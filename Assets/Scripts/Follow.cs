﻿using UnityEngine;
using System.Collections;

public class Follow : MonoBehaviour {
    public GameObject target;
    Vector3 lastPosition;
    Vector3 lastTargetPosition;

	// Update is called once per frame
    void start()
    {
        
    }
    void Update()
    {
        if (target != null)
            lastTargetPosition = target.transform.position;
    }
	void LateUpdate () {
        if (target == null)
            return;

        
        transform.position = transform.position + (target.transform.position - lastTargetPosition);
        //lastTargetPosition = target.transform.position;
        
    
	}
    void followTarget(GameObject target)
    {
        this.target = target;
    }
}
