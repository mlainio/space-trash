﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {
	public GameObject target;
	public float zoomStep = 5;  
    public float defaultZoomLevel;
	float zoomGoal;
	public float zoomSmoothing=30;
	public float minZoomLevel = 1;
    public float camMaxOffset = 30;
    public float camMoveSmooth = 0.5f;
    MouseCursor mouseCursor;
    public bool smart = true;
    public float maxZoomLevel = 10;
    public float smoothCompensate = 0.005f;
    public float zoomFactor = 2;
    public KeyCode mapKey = KeyCode.Tab;
    float oldZoomLevel;
    public float zoomLevel
    {
        get { return zoomFactor * Mathf.Log10(camera.orthographicSize); }
        set { camera.orthographicSize = Mathf.Pow(10, value / zoomFactor); }
    }
       
	// Use this for initialization
	void Start () {
		zoomGoal = zoomLevel;
        zoomLevel = defaultZoomLevel;
        zoomGoal = defaultZoomLevel;
        mouseCursor = GameHelper.getCursor();
   	}
	
	// Update is called once per frame
    void FixedUpdate()
    {
        if (smart) //trackTargetSmart();
            trackTargetSmart();
        else trackTargetCenter();
    }
    void Update() {
        
		handleZoom ();

	}
	void trackTargetSmart() {
        float z = transform.position.z;        
        Vector3 pos = target.transform.position + (mouseCursor.getWorldPosition() - target.transform.position) * 0.50f;
        pos.z = z;
       
        // Compensate for camera smoothing
        Vector2 offset = target.rigidbody2D.velocity.normalized * Mathf.Pow(target.rigidbody2D.velocity.magnitude,2)*smoothCompensate;
 
        transform.position = Vector3.Lerp(transform.position, pos+(Vector3)offset, camMoveSmooth);
    
	}
    void trackTargetCenter()
    {
        float z = transform.position.z;
        Vector3 pos = new Vector3(target.transform.position.x, target.transform.position.y, z);
        transform.position = pos;        
        //transform.position += (pos - transform.position) / camMoveSmooth;
    }
    void trackTargetBroken()
    {
        float speed = target.rigidbody2D.velocity.magnitude;
        float distance = speed / (speed + 10f) * camMaxOffset;
        float limit =speed/(speed+10)*10f;
        if (distance > limit)
            distance = limit;
        Vector2 dir = (Vector2) (mouseCursor.getWorldPosition()-target.transform.position);
        dir = dir.normalized * distance;

        Vector3 oldPos = transform.position; // need to store z axis
        Vector3 pos = new Vector3(target.transform.position.x+dir.x, target.transform.position.y + dir.y, oldPos.z);
        
        transform.position = Vector3.Lerp(oldPos, pos, camMoveSmooth);
        
        //transform.position += (pos - transform.position) / camMoveSmooth;
    }
	void handleZoom() {
        if (Input.GetKeyDown(mapKey))
        {
            if (zoomLevel < 14.5f)
            {
                oldZoomLevel = zoomLevel;
                zoomGoal = 15f;
            }
            else
                zoomGoal = 5;
                
        }
        float zoomMod = Input.GetAxis ("Mouse ScrollWheel");

        if (zoomMod != 0) {
            zoomGoal -= zoomMod*zoomStep;
        }

		if (zoomLevel != zoomGoal) {
				zoomLevel -= (zoomLevel - zoomGoal) * 1 / zoomSmoothing;

				if (zoomLevel < minZoomLevel) {
						zoomLevel = minZoomLevel;
						zoomGoal = minZoomLevel;
				}
                else if (zoomLevel > maxZoomLevel)
                {
                    zoomLevel = maxZoomLevel;
                    zoomGoal = maxZoomLevel;
                }				
		}	

	}
    
}
