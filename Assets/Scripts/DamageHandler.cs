﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VelocityTracker))]
public class DamageHandler : MonoBehaviour {
    
    class DamageText
    {
        public float dmg;
        public Vector3 pos;
        public float time;
        public Color color;

        public void addTime(float t)
        {
            time += t;
        }
        public void draw(Vector3 offset, GUIStyle style)
        {
            pos += offset;
            Rect r = new Rect(pos.x, Screen.height - pos.y, 30f, 30f);
            GUI.contentColor = color;            
            GUI.Label(r, ((int)(dmg*10)/10).ToString(), style);
            time -= Time.deltaTime;
        }
    }
    
    [SerializeField]
    public List<Armor> armorList = new List<Armor>();
	public AudioSource dieSound;
    public ParticleSystem death;
    public float deathDuration = 5f;
    //public List<GameObject> debrisList = new List<GameObject>();
    public List<GameObject> dieWithMe = new List<GameObject>();    
    //public float debrisRadius = 1f;
    //public int maxDebrisCount = 10;
        
    public bool destructible = true;
    public int health = 100;
    //public int healthMax;
    public int healthMax;
    private bool showHP = false;
    public bool showDmg = false;
    public float damageTextDuration = 2.0f;
    Texture2D healthBarHealthy;
    string defaultHealthy = "Textures/smallRedBar";
    Texture2D healthBarWounded;
    string defaultWounded = "Textures/smallBlackBar";
    
    List<DamageText> damageTextList = new List<DamageText>();

    bool dying = false;
    public float deathclock = 1f;
    public float finalDeathclock = 2f;
    bool finalDeath = false;    
    private ParticleSystem part;  
    Camera cam;
    private PlayerHUD hud;
    public bool collected = false;
    private float targetingFadeTimer;
    private Vector2 velocityWhenAlive;
    ObjectSpawner spawn;
    //public static float debrisSpawnForceMultiplier = 100f;    
    
    void Start()
    {        
        showHP = false;
        hud = GameObject.Find("GUI_Object").GetComponent<PlayerHUD>();

        if (healthBarHealthy == null)
        {
            healthBarHealthy = (Texture2D) Resources.Load(defaultHealthy.ToString());
        }
        if (healthBarWounded== null)
        {
            healthBarWounded = (Texture2D) Resources.Load(defaultWounded.ToString());
        }
        dieWithMe.Add(this.gameObject);
        healthMax = health;
        cam = GameHelper.getCamera();
        spawn = gameObject.GetComponent<ObjectSpawner>();
    }
    
    void Update() {
        if (targetingFadeTimer > 0)
        {
            targetingFadeTimer -= Time.deltaTime;
        }
        else
        {
            showHP = false;
        }
        
        if (!finalDeath && (health <= 0 || deathclock <= 0) ) {

            setToDestroy(finalDeathclock);
        }        
        
        if (dying)
        {
           deathclock -= Time.deltaTime;
        }

        if (finalDeath && deathclock <= 0)
        {
            killMe();
        }
        
       
    }

    private void killMe()
    {

        //gameObject.SetActive(false);        
        if (dieWithMe.Count > 0)
        {
            foreach (GameObject o in dieWithMe)
            {
                //o.SetActive(true);
                Destroy(o);
                //o.SetActive(true);
            }
        }
        //gameObject.SetActive(true);
        //Destroy(this.gameObject);
    }
    private void setToDestroy(float t)
    {
        if (dying == true)
        {
			if (dieSound != null) dieSound.Play();
            finalDeath = true;
            velocityWhenAlive = rigidbody2D.velocity;
            if (dieWithMe.Count > 0)
            {
                foreach (GameObject o in dieWithMe)
                {
                    //Renderer[] rends = o.GetComponentsInChildren<Renderer>();
                    foreach (Renderer r in o.GetComponentsInChildren<Renderer>())
                    {
                        if (r.GetType() == typeof(SpriteRenderer) || 
                            r.GetType() == typeof(MeshRenderer))
                        {
                            r.enabled = false;
                        }
                            
                            
                    }
                    foreach (Rigidbody2D r in o.GetComponentsInChildren<Rigidbody2D>())
                    {
                        r.isKinematic = false;
                    }
                    foreach (Collider2D c in o.GetComponentsInChildren<Collider2D>())
                    {
                        c.enabled = false;
                    }

                    //if (o.rigidbody2D != null)
                    //    o.rigidbody2D.isKinematic = true;
                    //if (o.collider2D != null)
                    //    o.collider2D.enabled = false;
                    
                }
            }

            BroadcastMessage("stopParticleLooping", true, SendMessageOptions.DontRequireReceiver);
            try
            {
                part = (ParticleSystem)GameObject.Instantiate((Object)death, transform.position + new Vector3(0f, 0f, -1f), transform.rotation);
                //part.Play();
                Destroy(part.gameObject, deathDuration);
            }
            catch (MissingReferenceException e) { }
            if (!collected) spawnDebris();
            
            
        }
        else
            dying = true;

        deathclock = t;      
                
    }

    private void spawnDebris()
    {
        if (spawn != null)
        {
            spawn.activate = true;
        }
    }
    Vector2 randomRadius(float radius)
    {
        return new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f)) * radius;

    }
 
    public void applyDamage(Damage damage) {
        if (destructible && this.transform != null)
        {
            //Debug.Log(LayerMask.LayerToName(gameObject.layer)+": Incoming " + damage.value + " " + damage.type + " damage");
            DamageText temp = handleDamage(damage);

            Vector3 extents;
            
            if (gameObject.renderer != null && gameObject.renderer.GetType() == typeof(SpriteRenderer))
            {
                SpriteRenderer spriterenderer = (SpriteRenderer)gameObject.renderer;
                extents = spriterenderer.sprite.bounds.extents;
            }
            else
                extents = new Vector3(transform.position.x, transform.position.y);

            try
            {
                temp.pos = cam.WorldToScreenPoint(transform.position + new Vector3(0f, extents.y, 0f));
                temp.pos.x += Random.Range(-20f, 20f);
                damageTextList.Add(temp);
            }
            catch (System.NullReferenceException e) { Debug.Log(e); }
        }
        
    }
    private DamageText handleDamage(Damage dmg)
    {
        DamageText temp = new DamageText();
        
        foreach (Armor a in armorList)
        {
            string t = ("Processing" + dmg.value + "Damage. ");
            dmg = a.processDamage(dmg);
            t += (dmg.value + " damage left.");
            //Debug.Log(t);
        }        

       
        temp.time = damageTextDuration;
        switch (dmg.type)
        {            
            case Damage.Type.Gravity:
                //Debug.Log("Applying gravity damage");
                rigidbody2D.AddForce(dmg.effectVector);
                temp.dmg = dmg.value;
                temp.color = Color.cyan;
                temp.time = 0;
                break;

            default:
                //Debug.Log(health);
                health -= (int) dmg.value;
                //Debug.Log(health);
                
                temp.dmg = dmg.value;
                temp.color = Color.red;
                break;            
        }

        

        return temp;
    }
    void OnGUI()
    {
        if (showHP)
        {
            //showHitpoints();
            showHealthBar();
            showArmorStatus();
        }
            
        if (showDmg)
            showDamageText();
    }
    void showHitpoints(Vector3 pos, float w, float h)
    {       
        Rect r = new Rect(pos.x, Screen.height - pos.y, w,h);
        GUI.contentColor = Color.white;
        GUI.Label(r, Mathf.Round(health).ToString());
    }
    void showHealthBar()
    {
        float hpleft = (float)health / (float)healthMax;
        hud.drawHealthBarAboveTarget(transform, healthBarWounded, healthBarHealthy, ((hpleft <=0) ? 0 : hpleft));
    }
    void showArmorStatus()
    {
        Vector3 position = cam.WorldToScreenPoint(transform.position);
        GUI.contentColor = Color.green;
        Rect r = new Rect(position.x+hud.healthBarWidth, Screen.height - position.y, 200,100);
        GUI.Label(r, armorString,hud.style);
        
    }
    void showDamageText()
    {
        if (damageTextList.Count <= 0)
            return;


        Vector3 offset;
        List<DamageText> removed = new List<DamageText>();
        
        foreach (DamageText t in damageTextList)
        {
            offset = new Vector3(Random.Range(-5f, 5f), Random.Range(-5f, 5f), 0f);
            t.draw(offset,hud.style);       
            
        }
        //Debug.Log(damageTextList.Count);
        damageTextList.RemoveAll(t => t.time <= 0);
        //Debug.Log(damageTextList.Count);
    }
    public void setCollected(bool collected)
    {
        this.collected = collected;
    }

    void OnMouseOver()
    {
        targetingFadeTimer = 10f;
        showHP = true;
    }
    //void OnMouseExit() {
        //argetingFadeTimer = 3f;
        //showHP = false;
    //}
    public string armorString
    {
        get
        {
            string s = "Type / mass / Abs / Ref\n";
            foreach (Armor a in armorList)
            {
                s += a.ToString()+"\n";
            }
            return s;
        }
    }
}
