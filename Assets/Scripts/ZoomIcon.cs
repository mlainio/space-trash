﻿using UnityEngine;
using System.Collections;

public class ZoomIcon : MonoBehaviour {

    public Texture2D icon;
    public bool showIcon;
    public int zoomThreshold = 100;
    public int maxThreshold = 100000;
    SpriteRenderer spriterenderer;
    bool objectDisabled = false;
	// Use this for initialization
	void Start () {
        spriterenderer = gameObject.GetComponent<SpriteRenderer>();
	}
	
	// Update is called once per frame
	void Update () {
        if (spriterenderer != null)
        {
            if (GameHelper.getCamera().orthographicSize > zoomThreshold && !objectDisabled)
            {
                disableObject(true);
            }
            else if (GameHelper.getCamera().orthographicSize < zoomThreshold && objectDisabled)
            {
                disableObject(false);
            }
        }        
	
	}
    void OnGUI()
    {   
        float camsize = GameHelper.getCamera().orthographicSize;
        if (icon != null && camsize >= zoomThreshold && (InitConfig.ignoreMaxZoom ? true : camsize <= maxThreshold))
        {
            Vector3 pos = GameHelper.getCamera().WorldToScreenPoint(transform.position);
            GUI.DrawTexture(new Rect(pos.x, Screen.height - pos.y, icon.width, icon.height), icon);
            
        }
    }
    //Disables an object's renderers, physics etc.
    public void disableObject(bool disable)
    {
        //todo: Disable physics, colliders and all relevant from children and so forth...
        objectDisabled = disable;
        spriterenderer.enabled = !disable;
    }
}
