﻿using UnityEngine;
using System.Collections;
/*
 * Instructions:
 * To add SimpleOrbit script to ParentObject you need to...
 * -Add to an empty GameObject, name it e.g. Orbiter
 * -make Orbiter a child of the ParentObject
 * -Create another empty object called OnOrbit, and make it child of ParentObject
 * -Add SimpleOrbit script to Orbiter
 * -Note that both objects (orbiting object and its target) require this script and same components!
 */ 
public class SimpleOrbit : MonoBehaviour {
    //Target to Orbit
    private Transform target;
    //Parent object to hold all orbiting objects
    private float radius;
    public float orbitSpeed = 5f;
    private static float minSpeed = 2f;
    private static float maxSpeed = 10f;
    public bool randomDirection = false;
    public float orbitRadiusCorrectionSpeed = 0.1f;
    public bool onOrbit = true;
    public bool neverBreakOrbit = false;
    public string targetTag = "Orbit";
    Vector3 orbitDesiredPosition;
    Vector2 lastPosition;
    
	// Use this for initialization
	void Start () {
        //target = targetObject.GetComponent<Transform>();
        //orbitSpeed = Random.Range(minSpeed, maxSpeed);
        //Find Nearest OBject With Tag! TODO: make into function!
        if (randomDirection)
            orbitSpeed = Mathf.Sign(Random.Range(-1f, 1f)) * orbitSpeed;

        if (onOrbit)
            findNearestOrbit();

	}
	
	// Update is called once per frame
    
	void FixedUpdate () {
        if (onOrbit && target != null)
        {
            //Vector3 direction = transform.position - target.position;
            //Vector3 position = target.position + Vector3.RotateTowards(direction, target.up, (orbitDirectionCW ? 1 : -1) * orbitSpeed, 1.0f) * radius;
            //transform.Translate(position*Time.deltaTime);
            //transform.Translate(position);
             //Movement            
            this.transform.RotateAround (target.position, target.forward, orbitSpeed * Time.deltaTime);
            orbitDesiredPosition = (this.transform.position - target.position).normalized * radius + target.position;
            this.transform.position = Vector3.Slerp(this.transform.position, orbitDesiredPosition, Time.deltaTime * orbitRadiusCorrectionSpeed);

            if (transform.parent != null)
            {
                Vector3 local = transform.localPosition;
                transform.parent.position = this.transform.position;
                transform.localPosition -= local;
            }
            
        }
	}
    void Update()
    {
        
        
    }
    void LateUpdate()
    {
        
        lastPosition = (Vector2)transform.position;
    }
    public void findNearestOrbit()
    {
        float nearestDistance = Mathf.Infinity;
        Transform nearestObject = null;

        GameObject[] taggedObjects = GameObject.FindGameObjectsWithTag(targetTag);
        //Debug.Log(gameObject.ha)
        //Debug.Log("Found " + taggedObjects.Length + " objects with tag: "+targetTag);
        foreach (GameObject o in taggedObjects)
        {
            if (o != transform.parent.gameObject)
            {
                
                float distance = (o.transform.position - transform.position).sqrMagnitude;
                float max = o.GetComponentInChildren<Orbit>().radius;
                //Debug.Log(o.GetInstanceID() + " Not Self, distance: "+distance);
                if (distance < nearestDistance && distance < max * max)
                {
                    nearestDistance = distance;
                    nearestObject = o.transform;
                }
            }
            //else
                //Debug.Log(o.GetInstanceID()+" Found Self");
            
        }

        
        if ( nearestObject != null )
        {
            //Debug.Log(gameObject+ " " +gameObject.GetInstanceID() + "Found Orbit at "+nearestObject+" "+nearestObject.GetInstanceID());
            target = nearestObject;
            Orbit orbit = target.gameObject.GetComponentInChildren<Orbit>();
            if (orbit != null)
            {                
                //Debug.Log("Setting up parent");
                //Debug.Log(transform.parent.parent);
                //Debug.Log(orbit.transform.GetInstanceID());
                orbit.receiverOrbiter(transform.parent);
                //Debug.Log(transform.parent.parent.GetInstanceID());
            }
            else
            {
                Debug.Log(gameObject + " " + gameObject.GetInstanceID() + "cannot find Orbit at " + nearestObject + " " + nearestObject.GetInstanceID());
            }
                

            radius = (this.transform.position - target.position).magnitude;
            //SendMessageUpwards("followTarget", nearestObject.gameObject,SendMessageOptions.DontRequireReceiver);
        }
    }
    public bool breakOrbit()
    {
        bool wasOnOrbit = false;
        if (!neverBreakOrbit)
        {
            wasOnOrbit = !onOrbit;         
            if (transform.parent.gameObject.rigidbody2D != null && transform.parent.gameObject.rigidbody2D.isKinematic == true)
            {
                transform.parent.gameObject.rigidbody2D.isKinematic = false;
                transform.parent.gameObject.rigidbody2D.velocity = this.velocity;
                wasOnOrbit = true;
            }
            onOrbit = false;
        }
            
        
        return wasOnOrbit;        
    }
    public void setOnOrbit()
    {
        onOrbit = true;
        findNearestOrbit();
        if (onOrbit)
        {
            if (gameObject.rigidbody2D != null && gameObject.rigidbody2D.isKinematic == false) gameObject.rigidbody2D.isKinematic = true;
        }
    }
    public Vector2 velocity
    {
        get
        {
            Vector2 velocity = (((Vector2)transform.position) - lastPosition) / Time.deltaTime;
            return velocity;
        }
    }
}
