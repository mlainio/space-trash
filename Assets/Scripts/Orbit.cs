﻿using UnityEngine;
using System.Collections;

public class Orbit : MonoBehaviour {
    public float radius = 1024;
    
	// Use this for initialization
    public void receiverOrbiter(Transform orbiter)
    {
        orbiter.parent = this.transform;
        if (orbiter.rigidbody2D != null)
            orbiter.rigidbody2D.isKinematic = true;
    }
}
