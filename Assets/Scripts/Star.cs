﻿using UnityEngine;
using System.Collections;

public class Star {

	public enum Type {M, K, G, F, A, B, O, Random};
    public enum Size {Dwarf, Main, Giant, Random};
    Color color;
    public Type type;
    public float radius;
    public float luminosity;
    public float temperature;
    public float mass;
    public float abundance;
    private static Star[] typeProperties = new Star[(int)Type.Random];
    private static Star[] sizeProperties = new Star[(int)Size.Random];
    public static float[] StarProbability;
    private static bool initialized = false;

    public Star() { }
    public Star(Type t, Color col, float r, float lum, float temp, float m, float abundance)
    {
        type = t;
        color = col;
        radius = r;
        luminosity = lum;
        temperature = temp;
        mass = m;
        this.abundance = abundance;
    }
    public static void initStars(){
        
        if (initialized)
            return;

        typeProperties[(int)Type.M] = new Star(Type.M, new Color(), 0.6f, 0.6f, 0.5f, 0.4f, 0.45f);
        typeProperties[(int)Type.K] = new Star(Type.K, new Color(), 0.8f, 0.8f, 0.8f, 0.7f, 0.20f);
        typeProperties[(int)Type.G] = new Star(Type.G, new Color(), 1.0f, 1.0f, 1.0f, 1.0f, 0.15f);
        typeProperties[(int)Type.F] = new Star(Type.F, new Color(), 1.3f, 1.5f, 1.15f, 1.5f, 0.12f);
        typeProperties[(int)Type.A] = new Star(Type.A, new Color(), 1.7f, 2f, 1.5f, 2.0f, 0.04f);
        typeProperties[(int)Type.B] = new Star(Type.B, new Color(), 5.0f, 4f, 3.5f, 10.0f, 0.02f);
        typeProperties[(int)Type.O] = new Star(Type.O, new Color(), 10.0f, 6f, 7.0f, 50.0f, 0.0001f);

        float[] t = new float[(int)Type.Random];
        for (int i = 0; i<(int)Type.Random;i++) {
            if (i == 0)
                t[i] = typeProperties[i].abundance;
            else
                t[i] = typeProperties[i].abundance + t[i - 1];
            //Debug.Log(t[i]);

        }
        StarProbability = new float[(int)Type.Random];
        for (int i = 0; i < (int)Type.Random; i++)
        {
            StarProbability[i] = t[i] / t[(int)Type.Random-1];
            //Debug.Log(StarProbability[i]);
        }

        //sizeProperties[(int)Size.Dwarf] = new Star(new Color(), 1f, 1f, 1f, 1f, 1f);
        //sizeProperties[(int)Size.Main] = new Star(new Color(), 1f, 1f, 1f, 1f, 1f);
        //sizeProperties[(int)Size.Giant] = new Star(new Color(), 1f, 1f, 1f, 1f, 1f);

        initialized = true;
    }
    public static Star operator *(Star left, Star right)
    {
        return new Star(left.type, left.color, left.radius * right.radius, left.luminosity * right.luminosity, left.temperature*right.temperature, left.mass  * right.mass, left.abundance);
    }       

    public static Star getProperties(Type t, Size s)
    {        
        return typeProperties[(int)t] * sizeProperties[(int)t];
    }
    public static Star getProperties(Type t)
    {
        int star;
        if (t == Type.Random)
        {
            float rand = Random.Range(0f,1f);
            int i = 0;
            while (rand > StarProbability[i])
            {
                i++;
            }
            star = i;
            Debug.Log("Random Star at P=" + rand + ": Found index " + i + "at P " + StarProbability[i]);
        }
        else
            star = (int)t;
        return typeProperties[star];
    }

}
