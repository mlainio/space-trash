﻿using UnityEngine;
using System.Collections.Generic;

public class Starfield : MonoBehaviour {
    public List<Color> starColors = new List<Color>();
    public int layers = 1;
    public int numStars = 64;
    public Texture2D starTexture;
    public Material starMaterial;
    public float scaleMax = 0.5f;
    public float scaleMin = 0.1f ;
    public float topLayerSpeed = 10f;
    public float bottomLayerSpeed = 0.1f;
    public float topAlpha = 1f;
    public float bottomAlpha = 0.2f;
    private float alphaStep;
    private float layerSpeedModifier;
    private int width;
    private int height;
    //private PlayerShipController playerShipControl;

    public class Star
    {
        public Vector2 position;
        public float scale;
        public Vector2 velocity;
        public Color color;
        
        public Star (Vector2 position, Vector2 velocity, float scale, Color color) {
            this.position = position;
            this.velocity = velocity;
            this.scale = scale;
            this.color = color;
        }
    }
    
    private Star[][] stars;
    private GameObject target;
    private VelocityTracker targetTracker;
	// Use this for initialization
	void Start () {
            
        //width = Screen.width;
        //height = Screen.height;
        width = Screen.width;
        height = Screen.height;
        layerSpeedModifier = (topLayerSpeed - bottomLayerSpeed) / layers;
        //playerShipControl = (PlayerShipController) GameObject.Find("PlayerShip").GetComponent<PlayerShipController>();
        if (GameHelper.getPlayer() == null)
        {
            target = GameHelper.getCamera().gameObject;
        }
        else
            target = GameHelper.getPlayer();

        targetTracker = target.GetComponent<VelocityTracker>();

        alphaStep = (topAlpha - bottomAlpha) / layers;
        
        //Initialize starfield
        stars = new Star[layers][];
        int i = 0;
        while (i < layers)
        {
            stars[i] = new Star[numStars];
            for (int j = 0; j<numStars; j++)
            {
                //int x = Screen.width/2 + (int) Random.Range(-width/2f, width/2f);
                //int y = Screen.height /2 + (int) Random.Range(-height/2f, height/2f);
                //Color col = new Color(Random.Range(0.2f,1f),Random.Range(0.2f,1f),Random.Range(0.2f,1f),Random.Range(0.4f, 1f));
                //stars[i][j] = new Star(new Vector2(x, y), new Vector2(0, 0), Random.Range(scaleMin, scaleMax),col);
                float alpha = topAlpha - alphaStep * i;
                stars[i][j] = createStar(alpha);
            }
            i++;
        }

	}
    
	
	// Update is called once per frame
    void Update()
    {
        Vector2 moveVector = targetTracker.averageVelocity();
        moveStarfield(moveVector);
    }

    private void moveStarfield(Vector2 move)
    {
        float speed = topLayerSpeed;
        for (int i = 0; i < layers; i++)
        {            
            foreach (Star s in stars[i])
            {
                //move stars opposite direction from move direction
                s.velocity.x = -move.x * speed;
                s.velocity.y = -move.y * speed;
                s.position += s.velocity * Time.deltaTime;

                //randomized position TODO
                if (starOutOfBounds(s))
                    respawn(s,false);
              
            }
            speed -= layerSpeedModifier;
        }
    }
    private Vector2 getCorrectedPosition(Vector2 position)
    {
        Vector2 correctedPosition = position;
        
        if (position.x < Screen.width / 2 - (width / 2))
        {            
            correctedPosition.x = Screen.width / 2 + width / 2;

            //Debug.Log("Screen: " + Screen.width + "(" + width + ")x" + Screen.height + "(" + height + ") X min->" + correctedPosition.x);
        }
        else if (position.x > Screen.width/2 + width / 2)
        {            
            correctedPosition.x = Screen.width / 2 - width / 2;
            //Debug.Log("Screen: " + Screen.width + "(" + width + ")x" + Screen.height + "(" + height + ") X Max->" + correctedPosition.x);
        }

        if (position.y < Screen.height / 2 - (height / 2))
        {
            //correctedPosition.x = Screen.width / 2 + width / 2 + outsideScreenBuffer;
            correctedPosition.y = Screen.height / 2 + height / 2;
            //Debug.Log("Screen: " + Screen.width + "(" + width + ")x" + Screen.height + "(" + height + ") Y Min->" + correctedPosition.y);
        }
        else if (position.y > Screen.height/2 + height/2 )
        {
            //correctedPosition.x = Screen.width / 2 - width / 2 - outsideScreenBuffer;
            correctedPosition.y = Screen.height / 2 - height / 2;
            //Debug.Log("Screen: " + Screen.width + "(" + width + ")x" + Screen.height + "(" + height + ") Y Max->" + correctedPosition.y);
        }

        if ( correctedPosition != position)
            return correctedPosition;

        return Vector2.zero;
        
    }
    private bool starOutOfBounds(Star s)
    {
        if (s.position.x < Screen.width / 2 - (width / 2) ||
            s.position.x > Screen.width/2 + width / 2 ||
            s.position.y < Screen.height / 2 - (height / 2 ) ||
            s.position.y > Screen.height / 2 + height / 2 )
        {
            return true;
        }
        return false;
    }
    private void respawn(Star s, bool randomized = true)
    {
        s.position = getCorrectedPosition(s.position);
        if (randomized)
        {
            //BROKEN SHIT TODO FIX IT!
            Vector2 center = new Vector2(width / 2, height / 2 );
            Vector2 bounds = new Vector2(width, height);
            Vector2 dir = s.velocity.normalized;
            Vector2 random = new Vector2();
            random.y = Random.Range(0, height);
            random.x = Random.Range(0, width);
            //if (dir.x < 0)

            if (dir.x > 0 || dir.x < 0)
                s.position.y = random.y;
            if (dir.y > 0 || dir.y < 0)
                s.position.x = random.x;

            
            //Debug.Log("New: "+s.position);
        }
            
    }
	void OnPostRender() {

        for (int i = 0; i < layers; i++)
        {
            foreach (Star s in stars[i])
            {
                //if (Event.current.type.Equals(EventType.Repaint))
                    drawStar(s);
            }
        }
    }
    private Star createStar(float alpha)
    {
        Color col;
        col = starColors[Random.Range(0, starColors.Count)];
        col.a = alpha;
        //col = new Color(
        //    Random.Range(0.2f, 1f),
        //    Random.Range(0.2f, 1f),
        //    Random.Range(0.2f, 1f),
        //    alpha);

        return new Star(randomizeLocation(), new Vector2(0, 0), Random.Range(scaleMin, scaleMax), col);
    }

    private Vector2 randomizeLocation()
    {
        int x = (
            Screen.width / 2 +
            (int)Random.Range(
                -width / 2f,
                width / 2f));

        int y = (
            Screen.height / 2 +
            (int)Random.Range(
                -height / 2f,
                height / 2f));

        return new Vector2(x, y);
    }
    private void drawStar(Star s)
    {
        starMaterial.SetColor("_TintColor", s.color);
        Rect pos = new Rect(s.position.x, s.position.y, starTexture.width * s.scale, starTexture.height * s.scale);
        GL.PushMatrix();
        GL.LoadPixelMatrix();
        Graphics.DrawTexture(pos, starTexture, starMaterial);
        GL.PopMatrix();
    }
}
