﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
public class ThrusterController : MonoBehaviour {
    public enum turnDirection { Left, ReverseRight, Right, ReverseLeft, Center };
    List<Thruster> Thrusterlist = new List<Thruster>();
	ShipController controller;
	TrackObject tracker;
    List<bool> thrusterCommands;
	public float deadangle;
	// Use this for initialization
	void Start () {
		getThrusters ();
		controller = transform.parent.gameObject.GetComponent<ShipController>();
		tracker = transform.parent.gameObject.GetComponent<TrackObject> ();
        thrusterCommands = new List<bool>((int)Thruster.Position.Total);
        //thrusterCommands.ForEach(t => t = false);
        for (int i = 0; i < (int)Thruster.Position.Total; i++)
            thrusterCommands.Add(false);

        Debug.Log(thrusterCommands.Count);       
    }

	public void getThrusters() {
		Thruster[] array = GetComponentsInChildren<Thruster>();
		foreach (Thruster t in array)
		{
			if (t.gameObject.tag == "Thruster")
			{
				Thrusterlist.Add(t);
				Debug.Log(Thrusterlist.Count + "Items in Thrusterlist");
			}
		}
	}

	// Update is called once per frame
	void Update() {
		
        updateThrusterCommands();

        for (int i = 0; i < (int)Thruster.Position.Total; i++ )
        {
            if (thrusterCommands[i])
            {
                Thrusterlist.ForEach(t => t.Thrust((Thruster.Position)i));
            }
            else
                Thrusterlist.ForEach(t => t.Cease((Thruster.Position)i));
        }
	}

    private void updateThrusterCommands()
    {
        Vector2 direction = controller.getLocalForceVector().normalized;
        //Debug.Log(direction);
        if (controller.slowdown())
        {
            for (int i = 0; i < (int)Thruster.Position.Total; i++)
            {
                thrusterCommands[i] = true;
            }
                thrusterCommands[(int)Thruster.Position.Main] = false;
            thrusterCommands[(int)Thruster.Position.Turbo] = false;
        }
        else
        {
            //Engage thrust forward
            if (direction.y > 0)
            {
                thrusterCommands[(int)Thruster.Position.Main] = true;
            }
            else
                thrusterCommands[(int)Thruster.Position.Main] = false;
            
            if (controller.isTurbo() && direction.y > 0)
            {
                thrusterCommands[(int)Thruster.Position.Turbo] = true;
            }
            else
            {
                thrusterCommands[(int)Thruster.Position.Turbo] = false;
            }
            
            
                // No reverse and no turning thrust
            if (tracker.angle >= -deadangle && tracker.angle <= deadangle)
            {
                thrusterCommands[(int)Thruster.Position.FrLeft] = false;
                thrusterCommands[(int)Thruster.Position.FrRight] = false;
                thrusterCommands[(int)Thruster.Position.AftLeft] = false;
                thrusterCommands[(int)Thruster.Position.AftRight] = false;
            }
                // Turning left
            else if (/*tracker.angle > deadangle && */(tracker.getTurnDirection() == turnDirection.Left/* || tracker.getTurnDirection() == turnDirection.ReverseLeft*/))
            {
                //Debug.Log ("Turning LEFT");
                thrusterCommands[(int)Thruster.Position.FrLeft] = true;
                thrusterCommands[(int)Thruster.Position.FrRight] = false;
                thrusterCommands[(int)Thruster.Position.AftLeft] = false;
                thrusterCommands[(int)Thruster.Position.AftRight] = true;
            }

                // Turning right
            else if (/*tracker.angle > deadangle &&*/ (tracker.getTurnDirection() == turnDirection.Right/* || tracker.getTurnDirection() == turnDirection.ReverseRight*/))
            {
                //Debug.Log ("Turning RIGHT");
                thrusterCommands[(int)Thruster.Position.FrLeft] = false;
                thrusterCommands[(int)Thruster.Position.FrRight] = true;
                thrusterCommands[(int)Thruster.Position.AftLeft] = true;
                thrusterCommands[(int)Thruster.Position.AftRight] = false;
            }
            //activate frontthrusters if going reverse;
            if (direction.y < 0)
            {
                thrusterCommands[(int)Thruster.Position.FrLeft] = true;
                thrusterCommands[(int)Thruster.Position.FrRight] = true;
            }
            //moving forward in aux mode
            if (direction.y > 0 && controller.isAuxMode())
            {
                thrusterCommands[(int)Thruster.Position.AftLeft] = true;
                thrusterCommands[(int)Thruster.Position.AftRight] = true;
            }
         

            // Left & Right thrusters (strafe)
            // Strafe right
            if (direction.x > 0)
            {
                thrusterCommands[(int)Thruster.Position.Left] = true;
                thrusterCommands[(int)Thruster.Position.Right] = false;
            }
            else if (direction.x < 0)
            {
                thrusterCommands[(int)Thruster.Position.Left] = false;
                thrusterCommands[(int)Thruster.Position.Right] = true;
            }
            else
            {
                thrusterCommands[(int)Thruster.Position.Left] = false;
                thrusterCommands[(int)Thruster.Position.Right] = false;
            }
        }
    }

}
